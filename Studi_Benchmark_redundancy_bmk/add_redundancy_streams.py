#!/usr/bin/env python3.8
import json
import os

directory="streams/"

for filename in os.listdir(directory):
    file = os.path.join(directory, filename)
    with open(file, encoding='utf-8') as f:
            jsondata = json.load(f)

    for (json_key, v) in jsondata.items():
        jsondata[json_key]["redundancy"]=1

    with open(file, 'w', encoding='utf-8') as f:
        json.dump(jsondata, f, ensure_ascii=False, indent=4,sort_keys=True)
