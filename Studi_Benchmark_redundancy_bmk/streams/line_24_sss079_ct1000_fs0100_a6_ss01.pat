{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e93"
            ],
            [
                "n23",
                "n22",
                "e89"
            ],
            [
                "n22",
                "n46",
                "e90"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n34",
                "e42"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n34",
                "e42"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n31",
                "e30"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n34",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n25",
                "e6"
            ]
        ],
        "sources": [
            "n34"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n43",
                "e78"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n20",
                "e76"
            ],
            [
                "n20",
                "n21",
                "e80"
            ],
            [
                "n21",
                "n45",
                "e86"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 279072,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n20",
                "e76"
            ],
            [
                "n20",
                "n21",
                "e80"
            ],
            [
                "n21",
                "n22",
                "e84"
            ],
            [
                "n22",
                "n46",
                "e90"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n42",
                "e74"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n32",
                "e34"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n32",
                "e34"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n30",
                "e26"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e93"
            ],
            [
                "n23",
                "n22",
                "e89"
            ],
            [
                "n22",
                "n21",
                "e85"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n31",
                "e30"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n41",
                "e70"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n33",
                "e38"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e2"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n34",
                "e42"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e2"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n33",
                "e38"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 250368,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n28",
                "e18"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n35",
                "e46"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e93"
            ],
            [
                "n23",
                "n22",
                "e89"
            ],
            [
                "n22",
                "n21",
                "e85"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 207312,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n20",
                "e76"
            ],
            [
                "n20",
                "n21",
                "e80"
            ],
            [
                "n21",
                "n22",
                "e84"
            ],
            [
                "n22",
                "n46",
                "e90"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n31",
                "e30"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n32",
                "e34"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n35",
                "e46"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 221664,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e2"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n31",
                "e30"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n28",
                "e18"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e85"
            ],
            [
                "n21",
                "n45",
                "e86"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 264720,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n43",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 279072,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n26",
                "e10"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 250368,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n20",
                "e76"
            ],
            [
                "n20",
                "n44",
                "e82"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e93"
            ],
            [
                "n23",
                "n22",
                "e89"
            ],
            [
                "n22",
                "n21",
                "e85"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n30",
                "e26"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n34",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n31",
                "e30"
            ]
        ],
        "sources": [
            "n34"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n26",
                "e10"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n30",
                "e26"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 279072,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n27",
                "e14"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 221664,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n42",
                "e74"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n29",
                "e22"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 221664,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n42",
                "e74"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e85"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 207312,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e2"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n27",
                "e14"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n20",
                "e76"
            ],
            [
                "n20",
                "n21",
                "e80"
            ],
            [
                "n21",
                "n45",
                "e86"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e93"
            ],
            [
                "n23",
                "n22",
                "e89"
            ],
            [
                "n22",
                "n46",
                "e90"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n27",
                "e14"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n27",
                "e14"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n20",
                "e76"
            ],
            [
                "n20",
                "n44",
                "e82"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n43",
                "e78"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e2"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n32",
                "e34"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n43",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n33",
                "e38"
            ]
        ],
        "sources": [
            "n43"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e85"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n43",
                "e78"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n31",
                "e30"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 236016,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n30",
                "e26"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 207312,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n26",
                "e10"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n33",
                "e38"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 250368,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n29",
                "e22"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n19",
                "e72"
            ],
            [
                "n19",
                "n20",
                "e76"
            ],
            [
                "n20",
                "n21",
                "e80"
            ],
            [
                "n21",
                "n45",
                "e86"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 207312,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n25",
                "e6"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 221664,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n42",
                "e74"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 307776,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e85"
            ],
            [
                "n21",
                "n20",
                "e81"
            ],
            [
                "n20",
                "n19",
                "e77"
            ],
            [
                "n19",
                "n18",
                "e73"
            ],
            [
                "n18",
                "n17",
                "e69"
            ],
            [
                "n17",
                "n16",
                "e65"
            ],
            [
                "n16",
                "n15",
                "e61"
            ],
            [
                "n15",
                "n14",
                "e57"
            ],
            [
                "n14",
                "n13",
                "e53"
            ],
            [
                "n13",
                "n12",
                "e49"
            ],
            [
                "n12",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n26",
                "e10"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n12",
                "e44"
            ],
            [
                "n12",
                "n13",
                "e48"
            ],
            [
                "n13",
                "n14",
                "e52"
            ],
            [
                "n14",
                "n15",
                "e56"
            ],
            [
                "n15",
                "n16",
                "e60"
            ],
            [
                "n16",
                "n17",
                "e64"
            ],
            [
                "n17",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n42",
                "e74"
            ]
        ],
        "sources": [
            "n31"
        ]
    }
}