{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n98"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n92",
                "n29",
                "e95"
            ],
            [
                "n29",
                "n28",
                "e91"
            ],
            [
                "n28",
                "n35",
                "e92"
            ],
            [
                "n35",
                "n98",
                "e120"
            ]
        ],
        "sources": [
            "n92"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n94"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n79",
                "n16",
                "e55"
            ],
            [
                "n16",
                "n17",
                "e56"
            ],
            [
                "n17",
                "n24",
                "e62"
            ],
            [
                "n24",
                "n31",
                "e82"
            ],
            [
                "n31",
                "n94",
                "e102"
            ]
        ],
        "sources": [
            "n79"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n92"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n105",
                "n42",
                "e133"
            ],
            [
                "n42",
                "n35",
                "e123"
            ],
            [
                "n35",
                "n28",
                "e93"
            ],
            [
                "n28",
                "n29",
                "e90"
            ],
            [
                "n29",
                "n92",
                "e94"
            ]
        ],
        "sources": [
            "n105"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n115"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n121",
                "n58",
                "e185"
            ],
            [
                "n58",
                "n59",
                "e186"
            ],
            [
                "n59",
                "n52",
                "e171"
            ],
            [
                "n52",
                "n115",
                "e168"
            ]
        ],
        "sources": [
            "n121"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n87"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n97",
                "n34",
                "e117"
            ],
            [
                "n34",
                "n33",
                "e115"
            ],
            [
                "n33",
                "n32",
                "e111"
            ],
            [
                "n32",
                "n31",
                "e105"
            ],
            [
                "n31",
                "n24",
                "e83"
            ],
            [
                "n24",
                "n87",
                "e80"
            ]
        ],
        "sources": [
            "n97"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n104"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n97",
                "n34",
                "e117"
            ],
            [
                "n34",
                "n41",
                "e118"
            ],
            [
                "n41",
                "n104",
                "e128"
            ]
        ],
        "sources": [
            "n97"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n119"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n94",
                "n31",
                "e103"
            ],
            [
                "n31",
                "n30",
                "e101"
            ],
            [
                "n30",
                "n29",
                "e97"
            ],
            [
                "n29",
                "n28",
                "e91"
            ],
            [
                "n28",
                "n35",
                "e92"
            ],
            [
                "n35",
                "n42",
                "e122"
            ],
            [
                "n42",
                "n49",
                "e136"
            ],
            [
                "n49",
                "n56",
                "e166"
            ],
            [
                "n56",
                "n119",
                "e176"
            ]
        ],
        "sources": [
            "n94"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n90"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n105",
                "n42",
                "e133"
            ],
            [
                "n42",
                "n35",
                "e123"
            ],
            [
                "n35",
                "n28",
                "e93"
            ],
            [
                "n28",
                "n29",
                "e90"
            ],
            [
                "n29",
                "n30",
                "e96"
            ],
            [
                "n30",
                "n31",
                "e100"
            ],
            [
                "n31",
                "n32",
                "e104"
            ],
            [
                "n32",
                "n33",
                "e110"
            ],
            [
                "n33",
                "n34",
                "e114"
            ],
            [
                "n34",
                "n27",
                "e87"
            ],
            [
                "n27",
                "n90",
                "e84"
            ]
        ],
        "sources": [
            "n105"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n109"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n10",
                "e18"
            ],
            [
                "n10",
                "n17",
                "e38"
            ],
            [
                "n17",
                "n24",
                "e62"
            ],
            [
                "n24",
                "n31",
                "e82"
            ],
            [
                "n31",
                "n38",
                "e106"
            ],
            [
                "n38",
                "n45",
                "e126"
            ],
            [
                "n45",
                "n46",
                "e148"
            ],
            [
                "n46",
                "n109",
                "e152"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n111",
                "n48",
                "e161"
            ],
            [
                "n48",
                "n41",
                "e131"
            ],
            [
                "n41",
                "n34",
                "e119"
            ],
            [
                "n34",
                "n27",
                "e87"
            ],
            [
                "n27",
                "n20",
                "e75"
            ],
            [
                "n20",
                "n13",
                "e43"
            ],
            [
                "n13",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n67",
                "e20"
            ]
        ],
        "sources": [
            "n111"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n120"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n115",
                "n52",
                "e169"
            ],
            [
                "n52",
                "n59",
                "e170"
            ],
            [
                "n59",
                "n58",
                "e187"
            ],
            [
                "n58",
                "n57",
                "e183"
            ],
            [
                "n57",
                "n120",
                "e180"
            ]
        ],
        "sources": [
            "n115"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n109"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n125",
                "n62",
                "e201"
            ],
            [
                "n62",
                "n55",
                "e175"
            ],
            [
                "n55",
                "n48",
                "e163"
            ],
            [
                "n48",
                "n47",
                "e159"
            ],
            [
                "n47",
                "n46",
                "e155"
            ],
            [
                "n46",
                "n109",
                "e152"
            ]
        ],
        "sources": [
            "n125"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n101",
                "n38",
                "e125"
            ],
            [
                "n38",
                "n31",
                "e107"
            ],
            [
                "n31",
                "n24",
                "e83"
            ],
            [
                "n24",
                "n17",
                "e63"
            ],
            [
                "n17",
                "n80",
                "e58"
            ]
        ],
        "sources": [
            "n101"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n107",
                "n44",
                "e143"
            ],
            [
                "n44",
                "n45",
                "e144"
            ],
            [
                "n45",
                "n38",
                "e127"
            ],
            [
                "n38",
                "n31",
                "e107"
            ],
            [
                "n31",
                "n24",
                "e83"
            ],
            [
                "n24",
                "n17",
                "e63"
            ],
            [
                "n17",
                "n80",
                "e58"
            ]
        ],
        "sources": [
            "n107"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n95"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n92",
                "n29",
                "e95"
            ],
            [
                "n29",
                "n30",
                "e96"
            ],
            [
                "n30",
                "n31",
                "e100"
            ],
            [
                "n31",
                "n32",
                "e104"
            ],
            [
                "n32",
                "n95",
                "e108"
            ]
        ],
        "sources": [
            "n92"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n123"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n70",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n14",
                "e34"
            ],
            [
                "n14",
                "n15",
                "e46"
            ],
            [
                "n15",
                "n16",
                "e52"
            ],
            [
                "n16",
                "n17",
                "e56"
            ],
            [
                "n17",
                "n24",
                "e62"
            ],
            [
                "n24",
                "n31",
                "e82"
            ],
            [
                "n31",
                "n38",
                "e106"
            ],
            [
                "n38",
                "n45",
                "e126"
            ],
            [
                "n45",
                "n52",
                "e150"
            ],
            [
                "n52",
                "n59",
                "e170"
            ],
            [
                "n59",
                "n60",
                "e190"
            ],
            [
                "n60",
                "n123",
                "e192"
            ]
        ],
        "sources": [
            "n70"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n115"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n119",
                "n56",
                "e177"
            ],
            [
                "n56",
                "n57",
                "e178"
            ],
            [
                "n57",
                "n58",
                "e182"
            ],
            [
                "n58",
                "n59",
                "e186"
            ],
            [
                "n59",
                "n52",
                "e171"
            ],
            [
                "n52",
                "n115",
                "e168"
            ]
        ],
        "sources": [
            "n119"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n93"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n115",
                "n52",
                "e169"
            ],
            [
                "n52",
                "n45",
                "e151"
            ],
            [
                "n45",
                "n38",
                "e127"
            ],
            [
                "n38",
                "n31",
                "e107"
            ],
            [
                "n31",
                "n30",
                "e101"
            ],
            [
                "n30",
                "n93",
                "e98"
            ]
        ],
        "sources": [
            "n115"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n66",
                "e14"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n76"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n120",
                "n57",
                "e181"
            ],
            [
                "n57",
                "n58",
                "e182"
            ],
            [
                "n58",
                "n59",
                "e186"
            ],
            [
                "n59",
                "n52",
                "e171"
            ],
            [
                "n52",
                "n45",
                "e151"
            ],
            [
                "n45",
                "n38",
                "e127"
            ],
            [
                "n38",
                "n31",
                "e107"
            ],
            [
                "n31",
                "n24",
                "e83"
            ],
            [
                "n24",
                "n17",
                "e63"
            ],
            [
                "n17",
                "n18",
                "e60"
            ],
            [
                "n18",
                "n19",
                "e66"
            ],
            [
                "n19",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n13",
                "e43"
            ],
            [
                "n13",
                "n76",
                "e40"
            ]
        ],
        "sources": [
            "n120"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n98"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n122",
                "n59",
                "e189"
            ],
            [
                "n59",
                "n52",
                "e171"
            ],
            [
                "n52",
                "n45",
                "e151"
            ],
            [
                "n45",
                "n44",
                "e145"
            ],
            [
                "n44",
                "n43",
                "e141"
            ],
            [
                "n43",
                "n42",
                "e135"
            ],
            [
                "n42",
                "n35",
                "e123"
            ],
            [
                "n35",
                "n98",
                "e120"
            ]
        ],
        "sources": [
            "n122"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n101"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n120",
                "n57",
                "e181"
            ],
            [
                "n57",
                "n58",
                "e182"
            ],
            [
                "n58",
                "n59",
                "e186"
            ],
            [
                "n59",
                "n52",
                "e171"
            ],
            [
                "n52",
                "n45",
                "e151"
            ],
            [
                "n45",
                "n38",
                "e127"
            ],
            [
                "n38",
                "n101",
                "e124"
            ]
        ],
        "sources": [
            "n120"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n119"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n105",
                "n42",
                "e133"
            ],
            [
                "n42",
                "n49",
                "e136"
            ],
            [
                "n49",
                "n56",
                "e166"
            ],
            [
                "n56",
                "n119",
                "e176"
            ]
        ],
        "sources": [
            "n105"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n70"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n81",
                "n18",
                "e65"
            ],
            [
                "n18",
                "n17",
                "e61"
            ],
            [
                "n17",
                "n16",
                "e57"
            ],
            [
                "n16",
                "n15",
                "e53"
            ],
            [
                "n15",
                "n14",
                "e47"
            ],
            [
                "n14",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n70",
                "e32"
            ]
        ],
        "sources": [
            "n81"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n108"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n121",
                "n58",
                "e185"
            ],
            [
                "n58",
                "n59",
                "e186"
            ],
            [
                "n59",
                "n52",
                "e171"
            ],
            [
                "n52",
                "n45",
                "e151"
            ],
            [
                "n45",
                "n108",
                "e146"
            ]
        ],
        "sources": [
            "n121"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n90"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n78",
                "n15",
                "e51"
            ],
            [
                "n15",
                "n16",
                "e52"
            ],
            [
                "n16",
                "n17",
                "e56"
            ],
            [
                "n17",
                "n18",
                "e60"
            ],
            [
                "n18",
                "n19",
                "e66"
            ],
            [
                "n19",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n27",
                "e74"
            ],
            [
                "n27",
                "n90",
                "e84"
            ]
        ],
        "sources": [
            "n78"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n81"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n63",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n10",
                "e18"
            ],
            [
                "n10",
                "n17",
                "e38"
            ],
            [
                "n17",
                "n18",
                "e60"
            ],
            [
                "n18",
                "n81",
                "e64"
            ]
        ],
        "sources": [
            "n63"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n118"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n79",
                "n16",
                "e55"
            ],
            [
                "n16",
                "n17",
                "e56"
            ],
            [
                "n17",
                "n18",
                "e60"
            ],
            [
                "n18",
                "n19",
                "e66"
            ],
            [
                "n19",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n27",
                "e74"
            ],
            [
                "n27",
                "n34",
                "e86"
            ],
            [
                "n34",
                "n41",
                "e118"
            ],
            [
                "n41",
                "n48",
                "e130"
            ],
            [
                "n48",
                "n55",
                "e162"
            ],
            [
                "n55",
                "n118",
                "e172"
            ]
        ],
        "sources": [
            "n79"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n107"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n87",
                "n24",
                "e81"
            ],
            [
                "n24",
                "n31",
                "e82"
            ],
            [
                "n31",
                "n38",
                "e106"
            ],
            [
                "n38",
                "n45",
                "e126"
            ],
            [
                "n45",
                "n44",
                "e145"
            ],
            [
                "n44",
                "n107",
                "e142"
            ]
        ],
        "sources": [
            "n87"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n77"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n90",
                "n27",
                "e85"
            ],
            [
                "n27",
                "n20",
                "e75"
            ],
            [
                "n20",
                "n19",
                "e71"
            ],
            [
                "n19",
                "n18",
                "e67"
            ],
            [
                "n18",
                "n17",
                "e61"
            ],
            [
                "n17",
                "n16",
                "e57"
            ],
            [
                "n16",
                "n15",
                "e53"
            ],
            [
                "n15",
                "n14",
                "e47"
            ],
            [
                "n14",
                "n77",
                "e44"
            ]
        ],
        "sources": [
            "n90"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n98"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n81",
                "n18",
                "e65"
            ],
            [
                "n18",
                "n17",
                "e61"
            ],
            [
                "n17",
                "n16",
                "e57"
            ],
            [
                "n16",
                "n15",
                "e53"
            ],
            [
                "n15",
                "n14",
                "e47"
            ],
            [
                "n14",
                "n21",
                "e48"
            ],
            [
                "n21",
                "n28",
                "e78"
            ],
            [
                "n28",
                "n35",
                "e92"
            ],
            [
                "n35",
                "n98",
                "e120"
            ]
        ],
        "sources": [
            "n81"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n82"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n69",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n13",
                "e30"
            ],
            [
                "n13",
                "n20",
                "e42"
            ],
            [
                "n20",
                "n19",
                "e71"
            ],
            [
                "n19",
                "n82",
                "e68"
            ]
        ],
        "sources": [
            "n69"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n104"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n76",
                "n13",
                "e41"
            ],
            [
                "n13",
                "n20",
                "e42"
            ],
            [
                "n20",
                "n27",
                "e74"
            ],
            [
                "n27",
                "n34",
                "e86"
            ],
            [
                "n34",
                "n41",
                "e118"
            ],
            [
                "n41",
                "n104",
                "e128"
            ]
        ],
        "sources": [
            "n76"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n73"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n98",
                "n35",
                "e121"
            ],
            [
                "n35",
                "n28",
                "e93"
            ],
            [
                "n28",
                "n21",
                "e79"
            ],
            [
                "n21",
                "n14",
                "e49"
            ],
            [
                "n14",
                "n15",
                "e46"
            ],
            [
                "n15",
                "n16",
                "e52"
            ],
            [
                "n16",
                "n17",
                "e56"
            ],
            [
                "n17",
                "n10",
                "e39"
            ],
            [
                "n10",
                "n73",
                "e36"
            ]
        ],
        "sources": [
            "n98"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n93"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n106",
                "n43",
                "e139"
            ],
            [
                "n43",
                "n42",
                "e135"
            ],
            [
                "n42",
                "n35",
                "e123"
            ],
            [
                "n35",
                "n28",
                "e93"
            ],
            [
                "n28",
                "n29",
                "e90"
            ],
            [
                "n29",
                "n30",
                "e96"
            ],
            [
                "n30",
                "n93",
                "e98"
            ]
        ],
        "sources": [
            "n106"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n105"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n124",
                "n61",
                "e197"
            ],
            [
                "n61",
                "n60",
                "e195"
            ],
            [
                "n60",
                "n59",
                "e191"
            ],
            [
                "n59",
                "n52",
                "e171"
            ],
            [
                "n52",
                "n45",
                "e151"
            ],
            [
                "n45",
                "n44",
                "e145"
            ],
            [
                "n44",
                "n43",
                "e141"
            ],
            [
                "n43",
                "n42",
                "e135"
            ],
            [
                "n42",
                "n105",
                "e132"
            ]
        ],
        "sources": [
            "n124"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n98",
                "n35",
                "e121"
            ],
            [
                "n35",
                "n28",
                "e93"
            ],
            [
                "n28",
                "n21",
                "e79"
            ],
            [
                "n21",
                "n14",
                "e49"
            ],
            [
                "n14",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n64",
                "e6"
            ]
        ],
        "sources": [
            "n98"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n87"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n69",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n10",
                "e18"
            ],
            [
                "n10",
                "n17",
                "e38"
            ],
            [
                "n17",
                "n24",
                "e62"
            ],
            [
                "n24",
                "n87",
                "e80"
            ]
        ],
        "sources": [
            "n69"
        ]
    }
}