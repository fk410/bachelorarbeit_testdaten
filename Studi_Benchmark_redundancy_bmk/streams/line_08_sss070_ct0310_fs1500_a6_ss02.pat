{
    "s000": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s001": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s002": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s003": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s004": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s005": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s006": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s007": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s008": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s009": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s010": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s011": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s012": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s013": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s014": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s015": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s016": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s017": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s018": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s019": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s020": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s021": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s022": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s023": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s024": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s025": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s026": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s027": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s028": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s029": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s030": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s031": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s032": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s033": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s034": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s035": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s036": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s037": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s038": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s039": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s040": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s041": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s042": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s043": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s044": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s045": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s046": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s047": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s048": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s049": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s050": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s051": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s052": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s053": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s054": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s055": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s056": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s057": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s058": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s059": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s060": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s061": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s062": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s063": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s064": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s065": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s066": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s067": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s068": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s069": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    }
}