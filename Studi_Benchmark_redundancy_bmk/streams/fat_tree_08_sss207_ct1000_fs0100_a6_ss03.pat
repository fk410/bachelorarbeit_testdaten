{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s079": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s080": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s081": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s082": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s083": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s084": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s085": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s086": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s087": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s088": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s089": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s090": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s091": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s092": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s093": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s094": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s095": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s096": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s097": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s098": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s099": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s100": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s101": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s102": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s103": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s104": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s105": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s106": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s107": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s108": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s109": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s110": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s111": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s112": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s113": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s114": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s115": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s116": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s117": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s118": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s119": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s120": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s121": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s122": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s123": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s124": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s125": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s126": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s127": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s128": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s129": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s130": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s131": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s132": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s133": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s134": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s135": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s136": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s137": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s138": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s139": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s140": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s141": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s142": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s143": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s144": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s145": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s146": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s147": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s148": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s149": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s150": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s151": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s152": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s153": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s154": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s155": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s156": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s157": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s158": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s159": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s160": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s161": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s162": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s163": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s164": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s165": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s166": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s167": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s168": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s169": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s170": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s171": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s172": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s173": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s174": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s175": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s176": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s177": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s178": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s179": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s180": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s181": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s182": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s183": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s184": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s185": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s186": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s187": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s188": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s189": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s190": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s191": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s192": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s193": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s194": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s195": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s196": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s197": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s198": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s199": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s200": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s201": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s202": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s203": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s204": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s205": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s206": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    }
}