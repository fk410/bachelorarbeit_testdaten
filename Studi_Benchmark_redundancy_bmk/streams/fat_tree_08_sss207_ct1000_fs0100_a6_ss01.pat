{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s079": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s080": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s081": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s082": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s083": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s084": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s085": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s086": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s087": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s088": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s089": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s090": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s091": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s092": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s093": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s094": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s095": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s096": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s097": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s098": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s099": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s100": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s101": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s102": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s103": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s104": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s105": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s106": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s107": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s108": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s109": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s110": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s111": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s112": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s113": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s114": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s115": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s116": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s117": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s118": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s119": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s120": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s121": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s122": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s123": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s124": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s125": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s126": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s127": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s128": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s129": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s130": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s131": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s132": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s133": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s134": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s135": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s136": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s137": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s138": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s139": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s140": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s141": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s142": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s143": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s144": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s145": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s146": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s147": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s148": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s149": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s150": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s151": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s152": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s153": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s154": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s155": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s156": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s157": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s158": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s159": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s160": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s161": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s162": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s163": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s164": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s165": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s166": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s167": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s168": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s169": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s170": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s171": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s172": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s173": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s174": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s175": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s176": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s177": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s178": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s179": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s180": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s181": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s182": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s183": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s184": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s185": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s186": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s187": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s188": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s189": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s190": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s191": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s192": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s193": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s194": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s195": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s196": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s197": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s198": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s199": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s200": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s201": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s202": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s203": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s204": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s205": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s206": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    }
}