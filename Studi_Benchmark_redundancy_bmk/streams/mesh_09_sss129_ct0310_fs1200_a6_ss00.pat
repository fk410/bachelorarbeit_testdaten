{
    "s000": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s001": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s002": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s003": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s004": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s005": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s006": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s007": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s008": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s009": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s010": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s011": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s012": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s013": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s014": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s015": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s016": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s017": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s018": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s019": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s020": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s021": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s022": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s023": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s024": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s025": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s026": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s027": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s028": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s029": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s030": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s031": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s032": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s033": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s034": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s035": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s036": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s037": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s038": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s039": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s040": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s041": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s042": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s043": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s044": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s045": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s046": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s047": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s048": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s049": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s050": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s051": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s052": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s053": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s054": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s055": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s056": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s057": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s058": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s059": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s060": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s061": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s062": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s063": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s064": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s065": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s066": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s067": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s068": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s069": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s070": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s071": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s072": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s073": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s074": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s075": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s076": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s077": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s078": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s079": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s080": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s081": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s082": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s083": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s084": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s085": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s086": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s087": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s088": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s089": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s090": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s091": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s092": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s093": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s094": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s095": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s096": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s097": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s098": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s099": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s100": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s101": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s102": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s103": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s104": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s105": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s106": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s107": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s108": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s109": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s110": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s111": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s112": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s113": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s114": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s115": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s116": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s117": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s118": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s119": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s120": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s121": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s122": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s123": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s124": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s125": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s126": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s127": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s128": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    }
}