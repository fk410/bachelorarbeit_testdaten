{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s079": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s080": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s081": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s082": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s083": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s084": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s085": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s086": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s087": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s088": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s089": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s090": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s091": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s092": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s093": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s094": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s095": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s096": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s097": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s098": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s099": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s100": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s101": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s102": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s103": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s104": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s105": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s106": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s107": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s108": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s109": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s110": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s111": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s112": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s113": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s114": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s115": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s116": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s117": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s118": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s119": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s120": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s121": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s122": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s123": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s124": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s125": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s126": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s127": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s128": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s129": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s130": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s131": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s132": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s133": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s134": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s135": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s136": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s137": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s138": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s139": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s140": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s141": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s142": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s143": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s144": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s145": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s146": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s147": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s148": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s149": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s150": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s151": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s152": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s153": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s154": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s155": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s156": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s157": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s158": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s159": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s160": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s161": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s162": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s163": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s164": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s165": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s166": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s167": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s168": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s169": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n29",
                "e56"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s170": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s171": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s172": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s173": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s174": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s175": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s176": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s177": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s178": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s179": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n14",
                "e71"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n23",
                "e82"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s180": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s181": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s182": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s183": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s184": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s185": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s186": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s187": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e76"
            ],
            [
                "n19",
                "n27",
                "e86"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s188": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e62"
            ],
            [
                "n15",
                "n26",
                "e72"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s189": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n25",
                "e58"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s190": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n10",
                "e57"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s191": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s192": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s193": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s194": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n18",
                "e85"
            ],
            [
                "n18",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s195": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s196": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s197": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e73"
            ],
            [
                "n15",
                "n12",
                "e63"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s198": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e74"
            ],
            [
                "n18",
                "n31",
                "e84"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s199": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e44"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s200": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e45"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s201": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e59"
            ],
            [
                "n11",
                "n8",
                "e49"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s202": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e55"
            ],
            [
                "n10",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n30",
                "e70"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s203": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s204": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e69"
            ],
            [
                "n14",
                "n12",
                "e61"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n28",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s205": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n16",
                "e77"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e46"
            ],
            [
                "n10",
                "n21",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s206": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n6",
                "e43"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n22",
                "e68"
            ]
        ],
        "sources": [
            "n28"
        ]
    }
}