{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n44",
                "e84"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n23",
                "e101"
            ],
            [
                "n23",
                "n22",
                "e99"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n16",
                "e73"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n10",
                "e45"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n43",
                "e80"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n25",
                "e0"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n26",
                "e6"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n43",
                "n18",
                "e81"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n42",
                "e76"
            ]
        ],
        "sources": [
            "n43"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n23",
                "e101"
            ],
            [
                "n23",
                "n22",
                "e99"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n26",
                "e6"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n32",
                "e32"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n22",
                "e97"
            ],
            [
                "n22",
                "n23",
                "e98"
            ],
            [
                "n23",
                "n24",
                "e102"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n32",
                "e32"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n10",
                "e45"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n27",
                "e10"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n24",
                "e105"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n43",
                "e80"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n23",
                "e101"
            ],
            [
                "n23",
                "n22",
                "e99"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n10",
                "e45"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n47",
                "e96"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n44",
                "e84"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n19",
                "e85"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n23",
                "e98"
            ],
            [
                "n23",
                "n24",
                "e102"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n23",
                "e103"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n24",
                "e105"
            ],
            [
                "n24",
                "n23",
                "e103"
            ],
            [
                "n23",
                "n22",
                "e99"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n45",
                "e88"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n42",
                "e76"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n36",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n27",
                "e10"
            ]
        ],
        "sources": [
            "n36"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n33",
                "e36"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n34",
                "n9",
                "e41"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n28",
                "e14"
            ]
        ],
        "sources": [
            "n34"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n17",
                "e77"
            ],
            [
                "n17",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n16",
                "e73"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n22",
                "e97"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n25",
                "e0"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n43",
                "e80"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n10",
                "e45"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n25",
                "e0"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n26",
                "e6"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n43",
                "n18",
                "e81"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n26",
                "e6"
            ]
        ],
        "sources": [
            "n43"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n26",
                "e6"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n19",
                "e82"
            ],
            [
                "n19",
                "n44",
                "e84"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n34",
                "n9",
                "e41"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n34"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n19",
                "e85"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n33",
                "e36"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n49",
                "e104"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n12",
                "e55"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n12",
                "e55"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n47",
                "e96"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n25",
                "e0"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n42",
                "e76"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n12",
                "e55"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n23",
                "e103"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n47",
                "e96"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n17",
                "e77"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n19",
                "e82"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n33",
                "e36"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n23",
                "e103"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n24",
                "e105"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n28",
                "e14"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n27",
                "e10"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n23",
                "e101"
            ],
            [
                "n23",
                "n24",
                "e102"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n33",
                "e36"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n19",
                "e85"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n23",
                "e98"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s079": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s080": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n22",
                "e97"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n32",
                "e32"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s081": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s082": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n24",
                "e105"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s083": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s084": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n43",
                "n18",
                "e81"
            ],
            [
                "n18",
                "n19",
                "e82"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n43"
        ]
    },
    "s085": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n32",
                "e32"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s086": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n36",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n44",
                "e84"
            ]
        ],
        "sources": [
            "n36"
        ]
    },
    "s087": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s088": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n47",
                "e96"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s089": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n25",
                "e0"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s090": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n43",
                "e80"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s091": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s092": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s093": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n17",
                "e77"
            ],
            [
                "n17",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s094": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n28",
                "e14"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s095": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n34",
                "n9",
                "e41"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n28",
                "e14"
            ]
        ],
        "sources": [
            "n34"
        ]
    },
    "s096": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s097": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n25",
                "e0"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s098": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s099": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n22",
                "e97"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n45",
                "e88"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s100": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s101": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n24",
                "e105"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s102": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s103": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n16",
                "e73"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n33",
                "e36"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s104": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s105": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s106": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n44",
                "e84"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s107": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n34",
                "n9",
                "e41"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n26",
                "e6"
            ]
        ],
        "sources": [
            "n34"
        ]
    },
    "s108": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s109": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n24",
                "e105"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n27",
                "e10"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s110": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s111": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n23",
                "e101"
            ],
            [
                "n23",
                "n24",
                "e102"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n32",
                "e32"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s112": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n36",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n36"
        ]
    }
}