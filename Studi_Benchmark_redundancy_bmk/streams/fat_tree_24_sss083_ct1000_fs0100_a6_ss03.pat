{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e132"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e207"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n47",
                "e178"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n18",
                "e153"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n63",
                "n12",
                "e129"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n63"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e209"
            ],
            [
                "n31",
                "n27",
                "e189"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e132"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e235"
            ],
            [
                "n37",
                "n33",
                "e215"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n67",
                "n36",
                "e233"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n67"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n18",
                "e153"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e235"
            ],
            [
                "n37",
                "n33",
                "e215"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e130"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e130"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e207"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n47",
                "e178"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n47",
                "e178"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e130"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e162"
            ],
            [
                "n25",
                "n53",
                "e182"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n50",
                "e256"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n50",
                "e256"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e162"
            ],
            [
                "n25",
                "n53",
                "e182"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e209"
            ],
            [
                "n31",
                "n27",
                "e189"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n20",
                "e138"
            ],
            [
                "n20",
                "n58",
                "e158"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e209"
            ],
            [
                "n31",
                "n27",
                "e189"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n48",
                "e204"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n20",
                "e138"
            ],
            [
                "n20",
                "n58",
                "e158"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e162"
            ],
            [
                "n25",
                "n53",
                "e182"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n47",
                "e178"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n47",
                "e178"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e235"
            ],
            [
                "n37",
                "n33",
                "e215"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s079": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s080": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e209"
            ],
            [
                "n31",
                "n27",
                "e189"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s081": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s082": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n47"
        ]
    }
}