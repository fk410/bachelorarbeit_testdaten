{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n36",
                "e231"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e132"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n20",
                "e138"
            ],
            [
                "n20",
                "n58",
                "e158"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n50",
                "e256"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n18",
                "e153"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n20",
                "e138"
            ],
            [
                "n20",
                "n58",
                "e158"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e259"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e132"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n25",
                "e162"
            ],
            [
                "n25",
                "n53",
                "e182"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e259"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n50",
                "e256"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n36",
                "e231"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n20",
                "e138"
            ],
            [
                "n20",
                "n58",
                "e158"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e132"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e259"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e235"
            ],
            [
                "n37",
                "n33",
                "e215"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n36",
                "e231"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e259"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n48",
                "e204"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n47",
                "e178"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e261"
            ],
            [
                "n43",
                "n39",
                "e241"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e209"
            ],
            [
                "n31",
                "n27",
                "e189"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n50",
                "e256"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n47",
                "e178"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e235"
            ],
            [
                "n37",
                "n33",
                "e215"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n63",
                "n12",
                "e129"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n63"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e259"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e235"
            ],
            [
                "n37",
                "n33",
                "e215"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e131"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n48",
                "e204"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n63",
                "n12",
                "e129"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e132"
            ]
        ],
        "sources": [
            "n63"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e235"
            ],
            [
                "n37",
                "n33",
                "e215"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n18",
                "e153"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e162"
            ],
            [
                "n25",
                "n53",
                "e182"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n36",
                "e231"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e130"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n48",
                "e204"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e205"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e162"
            ],
            [
                "n25",
                "n53",
                "e182"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s079": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s080": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n59",
                "n26",
                "e185"
            ],
            [
                "n26",
                "n21",
                "e165"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e130"
            ]
        ],
        "sources": [
            "n59"
        ]
    },
    "s081": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e207"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s082": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s083": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n50",
                "e256"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s084": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n67",
                "n36",
                "e233"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n67"
        ]
    },
    "s085": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n18",
                "e153"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s086": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s087": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n49",
                "e230"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s088": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n36",
                "e231"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e130"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s089": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e259"
            ],
            [
                "n42",
                "n50",
                "e256"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s090": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s091": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s092": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s093": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e207"
            ],
            [
                "n30",
                "n27",
                "e187"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s094": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e155"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s095": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s096": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s097": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n48",
                "e204"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s098": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e263"
            ],
            [
                "n44",
                "n39",
                "e243"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s099": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n31",
                "e188"
            ],
            [
                "n31",
                "n54",
                "e208"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s100": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e212"
            ],
            [
                "n36",
                "n67",
                "e232"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s101": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s102": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s103": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e181"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e242"
            ],
            [
                "n44",
                "n62",
                "e262"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s104": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e133"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e162"
            ],
            [
                "n25",
                "n53",
                "e182"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s105": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e216"
            ],
            [
                "n38",
                "n61",
                "e236"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s106": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e183"
            ],
            [
                "n25",
                "n21",
                "e163"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e190"
            ],
            [
                "n32",
                "n60",
                "e210"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s107": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e209"
            ],
            [
                "n31",
                "n27",
                "e189"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s108": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e211"
            ],
            [
                "n32",
                "n27",
                "e191"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e238"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s109": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n36",
                "e231"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e132"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s110": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e259"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s111": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e209"
            ],
            [
                "n31",
                "n27",
                "e189"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e160"
            ],
            [
                "n24",
                "n65",
                "e180"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s112": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n39",
                "e239"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s113": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n18",
                "e153"
            ],
            [
                "n18",
                "n15",
                "e135"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e130"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s114": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n59"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e164"
            ],
            [
                "n26",
                "n59",
                "e184"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s115": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n64",
                "e154"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s116": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e237"
            ],
            [
                "n38",
                "n33",
                "e217"
            ],
            [
                "n33",
                "n37",
                "e214"
            ],
            [
                "n37",
                "n55",
                "e234"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s117": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s118": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n67",
                "n36",
                "e233"
            ],
            [
                "n36",
                "n33",
                "e213"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e136"
            ],
            [
                "n19",
                "n52",
                "e156"
            ]
        ],
        "sources": [
            "n67"
        ]
    },
    "s119": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e134"
            ],
            [
                "n18",
                "n46",
                "e152"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s120": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e159"
            ],
            [
                "n20",
                "n15",
                "e139"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e186"
            ],
            [
                "n30",
                "n66",
                "e206"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s121": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e157"
            ],
            [
                "n19",
                "n15",
                "e137"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s122": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e257"
            ],
            [
                "n42",
                "n68",
                "e258"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s123": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n56"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e179"
            ],
            [
                "n24",
                "n21",
                "e161"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e240"
            ],
            [
                "n43",
                "n56",
                "e260"
            ]
        ],
        "sources": [
            "n47"
        ]
    }
}