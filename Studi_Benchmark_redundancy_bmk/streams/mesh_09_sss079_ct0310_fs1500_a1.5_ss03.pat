{
    "s000": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s001": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s002": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s003": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s004": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s005": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s006": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s007": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s008": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s009": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s010": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s011": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s012": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s013": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s014": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s015": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s016": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s017": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s018": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s019": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s020": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s021": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s022": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s023": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s024": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s025": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s026": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s027": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s028": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s029": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s030": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s031": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s032": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s033": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s034": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s035": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s036": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s037": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s038": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s039": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s040": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s041": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s042": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s043": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s044": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s045": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s046": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s047": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s048": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s049": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s050": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s051": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s052": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s053": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s054": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s055": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s056": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s057": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s058": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s059": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s060": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s061": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s062": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s063": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s064": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s065": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s066": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s067": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s068": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s069": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s070": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s071": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s072": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s073": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s074": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s075": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s076": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s077": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s078": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n15"
        ]
    }
}