{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n22",
                "e46"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n8",
                "e36"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n17",
                "e24"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n11",
                "e48"
            ],
            [
                "n11",
                "n8",
                "e41"
            ],
            [
                "n8",
                "n20",
                "e38"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n12",
                "e0"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e50"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n6",
                "e33"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e50"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n7",
                "e37"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e42"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n6",
                "e33"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n8",
                "e36"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n17",
                "e24"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n6",
                "e33"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n12",
                "e0"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n22",
                "e46"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n17",
                "e24"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e50"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n9",
                "e45"
            ],
            [
                "n9",
                "n6",
                "e33"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n20",
                "e38"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n20",
                "e38"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n9",
                "e45"
            ],
            [
                "n9",
                "n6",
                "e33"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n20",
                "e38"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n8",
                "e41"
            ],
            [
                "n8",
                "n7",
                "e37"
            ],
            [
                "n7",
                "n19",
                "e34"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n8",
                "e41"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n16",
                "e20"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n12",
                "e0"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n9",
                "e45"
            ],
            [
                "n9",
                "n21",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n12",
                "e0"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e42"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n8",
                "e41"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e0"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n8",
                "e36"
            ],
            [
                "n8",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e50"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n19",
                "e34"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n17",
                "e24"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n8",
                "e41"
            ],
            [
                "n8",
                "n7",
                "e37"
            ],
            [
                "n7",
                "n19",
                "e34"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n8",
                "e41"
            ],
            [
                "n8",
                "n7",
                "e37"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n18",
                "e28"
            ]
        ],
        "sources": [
            "n23"
        ]
    }
}