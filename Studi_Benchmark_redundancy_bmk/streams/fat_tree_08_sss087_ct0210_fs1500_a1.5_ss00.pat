{
    "s000": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s001": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s002": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s003": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s004": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s005": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s006": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s007": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s008": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s009": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s010": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s011": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s012": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s013": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s014": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s015": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s016": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s017": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s018": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s019": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s020": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s021": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s022": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s023": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s024": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s025": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s026": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s027": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s028": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s029": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s030": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s031": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s032": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s033": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s034": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s035": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s036": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s037": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s038": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s039": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s040": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s041": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s042": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s043": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s044": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s045": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s046": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s047": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s048": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s049": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s050": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s051": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s052": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s053": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s054": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s055": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s056": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s057": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s058": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s059": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s060": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s061": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s062": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s063": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s064": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s065": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n7",
                "e43"
            ],
            [
                "n7",
                "n4",
                "e35"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s066": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s067": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s068": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s069": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s070": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s071": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s072": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n26",
                "e66"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s073": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ],
            [
                "n8",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n25",
                "e54"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s074": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s075": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s076": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s077": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n18",
                "e68"
            ],
            [
                "n18",
                "n23",
                "e76"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s078": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s079": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n11",
                "e55"
            ],
            [
                "n11",
                "n8",
                "e47"
            ],
            [
                "n8",
                "n10",
                "e44"
            ],
            [
                "n10",
                "n21",
                "e52"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s080": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s081": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n6",
                "e32"
            ],
            [
                "n6",
                "n20",
                "e40"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s082": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s083": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n6",
                "e41"
            ],
            [
                "n6",
                "n4",
                "e33"
            ],
            [
                "n4",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n16",
                "e6"
            ],
            [
                "n16",
                "n19",
                "e70"
            ],
            [
                "n19",
                "n27",
                "e78"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s084": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n16",
                "e71"
            ],
            [
                "n16",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n4",
                "e0"
            ],
            [
                "n4",
                "n7",
                "e34"
            ],
            [
                "n7",
                "n24",
                "e42"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s085": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n12",
                "e59"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s086": {
        "cycle_time_ns": 210000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n10",
                "e53"
            ],
            [
                "n10",
                "n8",
                "e45"
            ],
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n12",
                "e4"
            ],
            [
                "n12",
                "n14",
                "e56"
            ],
            [
                "n14",
                "n22",
                "e64"
            ]
        ],
        "sources": [
            "n21"
        ]
    }
}