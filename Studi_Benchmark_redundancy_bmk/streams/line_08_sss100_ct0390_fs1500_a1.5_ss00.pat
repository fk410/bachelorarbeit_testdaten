{
    "s000": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s001": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s002": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s003": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s004": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s005": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s006": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s007": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s008": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s009": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s010": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s011": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s012": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s013": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s014": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 43512,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s015": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s016": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s017": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s018": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s019": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s020": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s021": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s022": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 47100,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s023": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 47100,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s024": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s025": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s026": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s027": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 47100,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s028": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s029": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s030": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s031": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s032": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s033": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s034": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s035": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 43512,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s036": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s037": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s038": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s039": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s040": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s041": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s042": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s043": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 47100,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s044": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s045": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s046": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s047": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 43512,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s048": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s049": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s050": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s051": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s052": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s053": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s054": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s055": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s056": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s057": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s058": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s059": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s060": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 47100,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s061": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s062": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 47100,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s063": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s064": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s065": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 47100,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s066": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s067": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s068": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s069": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s070": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 43512,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s071": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s072": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s073": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s074": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s075": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s076": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s077": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s078": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s079": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s080": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s081": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s082": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s083": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s084": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s085": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s086": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s087": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s088": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s089": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s090": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s091": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s092": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 32748,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s093": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 43512,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s094": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s095": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 39924,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s096": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 36336,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s097": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s098": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 29160,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s099": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 25572,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    }
}