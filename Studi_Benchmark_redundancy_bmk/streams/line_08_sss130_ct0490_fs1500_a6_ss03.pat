{
    "s000": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s001": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s002": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s003": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s004": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s005": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s006": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s007": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s008": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s009": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s010": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s011": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s012": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s013": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s014": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s015": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s016": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s017": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s018": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s019": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s020": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s021": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s022": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s023": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s024": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s025": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s026": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s027": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s028": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s029": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s030": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s031": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s032": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s033": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s034": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s035": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s036": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s037": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s038": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s039": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s040": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s041": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s042": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s043": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s044": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s045": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s046": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s047": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s048": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s049": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s050": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s051": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s052": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s053": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s054": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s055": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s056": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s057": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s058": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s059": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s060": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s061": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s062": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s063": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s064": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s065": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s066": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s067": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s068": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s069": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s070": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s071": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s072": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s073": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s074": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s075": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s076": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s077": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s078": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s079": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s080": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s081": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s082": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s083": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s084": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s085": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s086": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s087": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s088": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s089": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s090": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s091": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s092": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s093": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s094": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s095": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s096": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s097": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s098": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s099": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s100": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s101": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s102": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s103": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s104": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s105": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s106": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s107": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s108": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n11",
                "e14"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s109": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s110": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n10",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s111": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s112": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 174048,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s113": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s114": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s115": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n14",
                "e26"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s116": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s117": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s118": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s119": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s120": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s121": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 159696,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s122": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s123": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s124": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n13",
                "e22"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s125": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s126": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n12",
                "e18"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s127": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s128": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n9",
                "e6"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s129": {
        "cycle_time_ns": 490000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 188400,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e2"
            ]
        ],
        "sources": [
            "n15"
        ]
    }
}