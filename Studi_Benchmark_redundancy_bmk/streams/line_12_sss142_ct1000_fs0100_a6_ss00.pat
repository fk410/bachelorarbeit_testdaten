{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s070": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s071": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s072": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s073": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s074": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s075": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s076": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s077": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s078": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s079": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s080": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s081": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s082": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s083": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s084": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s085": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s086": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s087": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s088": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s089": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s090": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s091": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s092": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s093": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s094": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s095": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s096": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s097": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s098": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s099": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s100": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s101": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s102": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s103": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s104": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s105": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s106": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s107": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s108": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s109": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s110": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s111": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s112": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s113": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n19"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n19",
                "e30"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s114": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n21"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n21",
                "e38"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s115": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s116": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s117": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n23",
                "n11",
                "e45"
            ],
            [
                "n11",
                "n10",
                "e41"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n23"
        ]
    },
    "s118": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s119": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s120": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s121": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s122": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s123": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s124": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n18"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n18",
                "e26"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s125": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s126": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s127": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n20",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n20"
        ]
    },
    "s128": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s129": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s130": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n19",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n19"
        ]
    },
    "s131": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n16",
                "e18"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s132": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n13",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s133": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e4"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n15",
                "e14"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s134": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n23"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e8"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n11",
                "e40"
            ],
            [
                "n11",
                "n23",
                "e44"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s135": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s136": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n20"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n20",
                "e34"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s137": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n22",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e37"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n1",
                "e5"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n12",
                "e2"
            ]
        ],
        "sources": [
            "n22"
        ]
    },
    "s138": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n22"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e12"
            ],
            [
                "n4",
                "n5",
                "e16"
            ],
            [
                "n5",
                "n6",
                "e20"
            ],
            [
                "n6",
                "n7",
                "e24"
            ],
            [
                "n7",
                "n8",
                "e28"
            ],
            [
                "n8",
                "n9",
                "e32"
            ],
            [
                "n9",
                "n10",
                "e36"
            ],
            [
                "n10",
                "n22",
                "e42"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s139": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n18"
        ]
    },
    "s140": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n21",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e33"
            ],
            [
                "n8",
                "n7",
                "e29"
            ],
            [
                "n7",
                "n6",
                "e25"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n17",
                "e22"
            ]
        ],
        "sources": [
            "n21"
        ]
    },
    "s141": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n18",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e21"
            ],
            [
                "n5",
                "n4",
                "e17"
            ],
            [
                "n4",
                "n3",
                "e13"
            ],
            [
                "n3",
                "n2",
                "e9"
            ],
            [
                "n2",
                "n14",
                "e10"
            ]
        ],
        "sources": [
            "n18"
        ]
    }
}