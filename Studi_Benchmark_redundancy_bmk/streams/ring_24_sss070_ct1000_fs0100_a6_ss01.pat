{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n27",
                "e16"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n12",
                "e46"
            ],
            [
                "n12",
                "n13",
                "e50"
            ],
            [
                "n13",
                "n14",
                "e54"
            ],
            [
                "n14",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n16",
                "e62"
            ],
            [
                "n16",
                "n40",
                "e68"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n11",
                "e49"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n47",
                "e94"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n12",
                "e46"
            ],
            [
                "n12",
                "n13",
                "e50"
            ],
            [
                "n13",
                "n37",
                "e56"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n38",
                "e60"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e61"
            ],
            [
                "n14",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n16",
                "e62"
            ],
            [
                "n16",
                "n17",
                "e66"
            ],
            [
                "n17",
                "n18",
                "e70"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n20",
                "e78"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n24",
                "e4"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n21",
                "e89"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n24",
                "e4"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n33",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e61"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n32",
                "e36"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n35",
                "e48"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n37",
                "e56"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n38",
                "e60"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e85"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n29",
                "e24"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n11",
                "e49"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n30",
                "e28"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n35",
                "e48"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e95"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n38",
                "e60"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e65"
            ],
            [
                "n15",
                "n16",
                "e62"
            ],
            [
                "n16",
                "n17",
                "e66"
            ],
            [
                "n17",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n44",
                "e84"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n34",
                "e44"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e85"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n34",
                "e44"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n33",
                "e40"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e65"
            ],
            [
                "n15",
                "n16",
                "e62"
            ],
            [
                "n16",
                "n17",
                "e66"
            ],
            [
                "n17",
                "n18",
                "e70"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n20",
                "e78"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e95"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n32",
                "e36"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 121200,
        "redundancy": 1,
        "route": [
            [
                "n43",
                "n19",
                "e81"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n36",
                "e52"
            ]
        ],
        "sources": [
            "n43"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n47",
                "e94"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n36",
                "n12",
                "e53"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n33",
                "e40"
            ]
        ],
        "sources": [
            "n36"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n36",
                "n12",
                "e53"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e4"
            ]
        ],
        "sources": [
            "n36"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n47",
                "e94"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n28",
                "e20"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n28",
                "e20"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e4"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 164256,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n17",
                "e66"
            ],
            [
                "n17",
                "n18",
                "e70"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n20",
                "e78"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n45",
                "e88"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n13",
                "e57"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n31",
                "e32"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n17",
                "e73"
            ],
            [
                "n17",
                "n18",
                "e70"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n20",
                "e78"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 192960,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n39",
                "e64"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n27",
                "e16"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n28",
                "e20"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n28",
                "e20"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n14",
                "e61"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n29",
                "e24"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e65"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n28",
                "e20"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n25"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n25",
                "e8"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n35",
                "e48"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n37",
                "e56"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n30",
                "e28"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n33",
                "e40"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n16",
                "e69"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n36",
                "e52"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n24",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n23",
                "e2"
            ],
            [
                "n23",
                "n22",
                "e91"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n45",
                "e88"
            ]
        ],
        "sources": [
            "n24"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e4"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n23",
                "e95"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e85"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n43"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n15",
                "e65"
            ],
            [
                "n15",
                "n16",
                "e62"
            ],
            [
                "n16",
                "n17",
                "e66"
            ],
            [
                "n17",
                "n18",
                "e70"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n43",
                "e80"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n17",
                "e73"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n15",
                "e63"
            ],
            [
                "n15",
                "n14",
                "e59"
            ],
            [
                "n14",
                "n13",
                "e55"
            ],
            [
                "n13",
                "n12",
                "e51"
            ],
            [
                "n12",
                "n11",
                "e47"
            ],
            [
                "n11",
                "n10",
                "e43"
            ],
            [
                "n10",
                "n9",
                "e39"
            ],
            [
                "n9",
                "n8",
                "e35"
            ],
            [
                "n8",
                "n32",
                "e36"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n21",
                "e89"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n12",
                "e46"
            ],
            [
                "n12",
                "n13",
                "e50"
            ],
            [
                "n13",
                "n14",
                "e54"
            ],
            [
                "n14",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n39",
                "e64"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n20",
                "e85"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n26",
                "e12"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n12",
                "e46"
            ],
            [
                "n12",
                "n13",
                "e50"
            ],
            [
                "n13",
                "n37",
                "e56"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 106848,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n22",
                "e93"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n16",
                "e67"
            ],
            [
                "n16",
                "n40",
                "e68"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 92496,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n22",
                "e93"
            ],
            [
                "n22",
                "n21",
                "e87"
            ],
            [
                "n21",
                "n20",
                "e83"
            ],
            [
                "n20",
                "n19",
                "e79"
            ],
            [
                "n19",
                "n18",
                "e75"
            ],
            [
                "n18",
                "n17",
                "e71"
            ],
            [
                "n17",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 178608,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n18",
                "e77"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n20",
                "e78"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n23",
                "e90"
            ],
            [
                "n23",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n29",
                "e24"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 35088,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n21",
                "e89"
            ],
            [
                "n21",
                "n22",
                "e86"
            ],
            [
                "n22",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 149904,
        "redundancy": 1,
        "route": [
            [
                "n36",
                "n12",
                "e53"
            ],
            [
                "n12",
                "n13",
                "e50"
            ],
            [
                "n13",
                "n14",
                "e54"
            ],
            [
                "n14",
                "n15",
                "e58"
            ],
            [
                "n15",
                "n16",
                "e62"
            ],
            [
                "n16",
                "n17",
                "e66"
            ],
            [
                "n17",
                "n18",
                "e70"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n20",
                "e78"
            ],
            [
                "n20",
                "n21",
                "e82"
            ],
            [
                "n21",
                "n45",
                "e88"
            ]
        ],
        "sources": [
            "n36"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 135552,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n8",
                "e30"
            ],
            [
                "n8",
                "n9",
                "e34"
            ],
            [
                "n9",
                "n10",
                "e38"
            ],
            [
                "n10",
                "n11",
                "e42"
            ],
            [
                "n11",
                "n12",
                "e46"
            ],
            [
                "n12",
                "n13",
                "e50"
            ],
            [
                "n13",
                "n14",
                "e54"
            ],
            [
                "n14",
                "n38",
                "e60"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s068": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n24"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n24",
                "e4"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s069": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n44"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 63792,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n17",
                "e73"
            ],
            [
                "n17",
                "n18",
                "e70"
            ],
            [
                "n18",
                "n19",
                "e74"
            ],
            [
                "n19",
                "n20",
                "e78"
            ],
            [
                "n20",
                "n44",
                "e84"
            ]
        ],
        "sources": [
            "n41"
        ]
    }
}