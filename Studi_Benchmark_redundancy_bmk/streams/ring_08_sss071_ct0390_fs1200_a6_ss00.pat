{
    "s000": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s001": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s002": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s003": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s004": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s005": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s006": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s007": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s008": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s009": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s010": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n11",
                "e16"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s011": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s012": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s013": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s014": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s015": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s016": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e8"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s017": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s018": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s019": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n8",
                "e4"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s020": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s021": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n11",
                "e16"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s022": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s023": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s024": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e4"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s025": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e4"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s026": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s027": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s028": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s029": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s030": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s031": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s032": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s033": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n9",
                "e8"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s034": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s035": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s036": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s037": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n11",
                "e16"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s038": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s039": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s040": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n11",
                "e16"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s041": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s042": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s043": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s044": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s045": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s046": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s047": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n11",
                "e16"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s048": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s049": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s050": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s051": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s052": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e4"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s053": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s054": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s055": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n8"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n8",
                "e4"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s056": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n2",
                "e6"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s057": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n11",
                "e16"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s058": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n3",
                "e10"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s059": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 130944,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n9",
                "e8"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s060": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n9",
                "e8"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s061": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n10",
                "e12"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s062": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n7",
                "e31"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s063": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n12",
                "e20"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s064": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 87888,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s065": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s066": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 102240,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n15",
                "e30"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s067": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n8",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n7",
                "e2"
            ],
            [
                "n7",
                "n6",
                "e27"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n13",
                "e24"
            ]
        ],
        "sources": [
            "n8"
        ]
    },
    "s068": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n4",
                "e14"
            ],
            [
                "n4",
                "n5",
                "e18"
            ],
            [
                "n5",
                "n6",
                "e22"
            ],
            [
                "n6",
                "n14",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s069": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e26"
            ],
            [
                "n7",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n1",
                "e0"
            ],
            [
                "n1",
                "n9",
                "e8"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s070": {
        "cycle_time_ns": 390000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1200,
        "max_latency_ns": 116592,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n11",
                "e16"
            ]
        ],
        "sources": [
            "n14"
        ]
    }
}