{
    "s000": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s001": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s002": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s003": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s004": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s005": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s006": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s007": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s008": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s009": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n14"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n14",
                "e24"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s010": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s011": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s012": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s013": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s014": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s015": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s016": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s017": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s018": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s019": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s020": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s021": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s022": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s023": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s024": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s025": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s026": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s027": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n5",
                "e22"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s028": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n15"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n15",
                "e28"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s029": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s030": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s031": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s032": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s033": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s034": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s035": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s036": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s037": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n16"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n6",
                "e18"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n16",
                "e32"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s038": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s039": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s040": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s041": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n4",
                "e23"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s042": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n13",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n13"
        ]
    },
    "s043": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s044": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n17"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n11",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n5",
                "e12"
            ],
            [
                "n5",
                "n8",
                "e26"
            ],
            [
                "n8",
                "n17",
                "e36"
            ]
        ],
        "sources": [
            "n11"
        ]
    },
    "s045": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n12"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n12",
                "e14"
            ]
        ],
        "sources": [
            "n9"
        ]
    },
    "s046": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n14",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n14"
        ]
    },
    "s047": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s048": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s049": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 102288,
        "redundancy": 1,
        "route": [
            [
                "n12",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n12"
        ]
    },
    "s050": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 145344,
        "redundancy": 1,
        "route": [
            [
                "n16",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n16"
        ]
    },
    "s051": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n13"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n10",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n3",
                "e4"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n13",
                "e20"
            ]
        ],
        "sources": [
            "n10"
        ]
    },
    "s052": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n9"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n15",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n3",
                "e19"
            ],
            [
                "n3",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ]
        ],
        "sources": [
            "n15"
        ]
    },
    "s053": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n10"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 130992,
        "redundancy": 1,
        "route": [
            [
                "n17",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n10",
                "e6"
            ]
        ],
        "sources": [
            "n17"
        ]
    },
    "s054": {
        "cycle_time_ns": 310000,
        "destinations": [
            "n11"
        ],
        "frame_size_b": 1500,
        "max_latency_ns": 116640,
        "redundancy": 1,
        "route": [
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n11",
                "e10"
            ]
        ],
        "sources": [
            "n9"
        ]
    }
}