{
    "s000": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n32"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n7",
                "e30"
            ],
            [
                "n7",
                "n32",
                "e32"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s001": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 53424,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n22",
                "e97"
            ],
            [
                "n22",
                "n21",
                "e95"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s002": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s003": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 53424,
        "redundancy": 1,
        "route": [
            [
                "n36",
                "n11",
                "e51"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n23",
                "e98"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n36"
        ]
    },
    "s004": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n42",
                "e76"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s005": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n12",
                "e55"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n42",
                "e76"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s006": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n47",
                "e96"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s007": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n49",
                "e104"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s008": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n23",
                "e101"
            ],
            [
                "n23",
                "n24",
                "e102"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s009": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s010": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 17544,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s011": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s012": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 17544,
        "redundancy": 1,
        "route": [
            [
                "n42",
                "n17",
                "e77"
            ],
            [
                "n17",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n42"
        ]
    },
    "s013": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n29",
                "n4",
                "e19"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n27",
                "e10"
            ]
        ],
        "sources": [
            "n29"
        ]
    },
    "s014": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n12",
                "e55"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n46",
                "e92"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s015": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s016": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n19",
                "e82"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n49",
                "e104"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s017": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n27",
                "e10"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s018": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n27"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 53424,
        "redundancy": 1,
        "route": [
            [
                "n43",
                "n18",
                "e81"
            ],
            [
                "n18",
                "n19",
                "e82"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n27",
                "e10"
            ]
        ],
        "sources": [
            "n43"
        ]
    },
    "s019": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n28",
                "e14"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s020": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s021": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 67776,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n47",
                "e96"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s022": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 17544,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n10",
                "e45"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s023": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 17544,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n22",
                "e97"
            ],
            [
                "n22",
                "n23",
                "e98"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s024": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 53424,
        "redundancy": 1,
        "route": [
            [
                "n26",
                "n1",
                "e7"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n26"
        ]
    },
    "s025": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n8",
                "e34"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s026": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s027": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n16",
                "e73"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n19",
                "e82"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s028": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s029": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s030": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n40"
        ]
    },
    "s031": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n19",
                "e85"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s032": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n39"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n16",
                "e73"
            ],
            [
                "n16",
                "n17",
                "e74"
            ],
            [
                "n17",
                "n18",
                "e78"
            ],
            [
                "n18",
                "n19",
                "e82"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n39",
                "e62"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s033": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n10",
                "e45"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s034": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n33",
                "n8",
                "e37"
            ],
            [
                "n8",
                "n9",
                "e38"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n42",
                "e76"
            ]
        ],
        "sources": [
            "n33"
        ]
    },
    "s035": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n30",
                "n5",
                "e23"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n30"
        ]
    },
    "s036": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n26"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n2",
                "e13"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n26",
                "e6"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s037": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n43",
                "n18",
                "e81"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n16",
                "e75"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n43"
        ]
    },
    "s038": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n34"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n19",
                "e85"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n34",
                "e40"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s039": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n30"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n30",
                "e22"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s040": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n44",
                "n19",
                "e85"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n23",
                "e103"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n44"
        ]
    },
    "s041": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 17544,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n45",
                "e88"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s042": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s043": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n38"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n38",
                "e58"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s044": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n39",
                "n14",
                "e63"
            ],
            [
                "n14",
                "n13",
                "e61"
            ],
            [
                "n13",
                "n12",
                "e57"
            ],
            [
                "n12",
                "n11",
                "e53"
            ],
            [
                "n11",
                "n10",
                "e47"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n39"
        ]
    },
    "s045": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s046": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s047": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n23",
                "e98"
            ],
            [
                "n23",
                "n24",
                "e102"
            ],
            [
                "n24",
                "n49",
                "e104"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s048": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n14",
                "e42"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n49",
                "e104"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s049": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n37",
                "n12",
                "e55"
            ],
            [
                "n12",
                "n13",
                "e56"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n24",
                "e86"
            ],
            [
                "n24",
                "n23",
                "e103"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n37"
        ]
    },
    "s050": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n37"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n12",
                "e52"
            ],
            [
                "n12",
                "n37",
                "e54"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s051": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n35"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n41",
                "n16",
                "e73"
            ],
            [
                "n16",
                "n15",
                "e69"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n35",
                "e44"
            ]
        ],
        "sources": [
            "n41"
        ]
    },
    "s052": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s053": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n35",
                "n10",
                "e45"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n35"
        ]
    },
    "s054": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n42"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n19",
                "e64"
            ],
            [
                "n19",
                "n18",
                "e83"
            ],
            [
                "n18",
                "n17",
                "e79"
            ],
            [
                "n17",
                "n42",
                "e76"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s055": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s056": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n33"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n28",
                "n3",
                "e15"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n9",
                "e20"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n33",
                "e36"
            ]
        ],
        "sources": [
            "n28"
        ]
    },
    "s057": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n40"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n40",
                "e66"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s058": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n41"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 31896,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n21",
                "e93"
            ],
            [
                "n21",
                "n20",
                "e91"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n16",
                "e68"
            ],
            [
                "n16",
                "n41",
                "e72"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s059": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 53424,
        "redundancy": 1,
        "route": [
            [
                "n49",
                "n24",
                "e105"
            ],
            [
                "n24",
                "n19",
                "e87"
            ],
            [
                "n19",
                "n14",
                "e65"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n8",
                "e39"
            ],
            [
                "n8",
                "n7",
                "e35"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n49"
        ]
    },
    "s060": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n32",
                "n7",
                "e33"
            ],
            [
                "n7",
                "n6",
                "e31"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n32"
        ]
    },
    "s061": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s062": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n31"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n27",
                "n2",
                "e11"
            ],
            [
                "n2",
                "n1",
                "e9"
            ],
            [
                "n1",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n6",
                "e24"
            ],
            [
                "n6",
                "n31",
                "e28"
            ]
        ],
        "sources": [
            "n27"
        ]
    },
    "s063": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n29"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 67776,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n20",
                "e89"
            ],
            [
                "n20",
                "n15",
                "e71"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n5",
                "e27"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n4",
                "e16"
            ],
            [
                "n4",
                "n29",
                "e18"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s064": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 60600,
        "redundancy": 1,
        "route": [
            [
                "n25",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n5",
                "e4"
            ],
            [
                "n5",
                "n10",
                "e26"
            ],
            [
                "n10",
                "n15",
                "e48"
            ],
            [
                "n15",
                "n20",
                "e70"
            ],
            [
                "n20",
                "n21",
                "e90"
            ],
            [
                "n21",
                "n22",
                "e94"
            ],
            [
                "n22",
                "n23",
                "e98"
            ],
            [
                "n23",
                "n48",
                "e100"
            ]
        ],
        "sources": [
            "n25"
        ]
    },
    "s065": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 46248,
        "redundancy": 1,
        "route": [
            [
                "n31",
                "n6",
                "e29"
            ],
            [
                "n6",
                "n5",
                "e25"
            ],
            [
                "n5",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n1",
                "e2"
            ],
            [
                "n1",
                "n2",
                "e8"
            ],
            [
                "n2",
                "n3",
                "e12"
            ],
            [
                "n3",
                "n28",
                "e14"
            ]
        ],
        "sources": [
            "n31"
        ]
    },
    "s066": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n28"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 39072,
        "redundancy": 1,
        "route": [
            [
                "n38",
                "n13",
                "e59"
            ],
            [
                "n13",
                "n14",
                "e60"
            ],
            [
                "n14",
                "n9",
                "e43"
            ],
            [
                "n9",
                "n4",
                "e21"
            ],
            [
                "n4",
                "n3",
                "e17"
            ],
            [
                "n3",
                "n28",
                "e14"
            ]
        ],
        "sources": [
            "n38"
        ]
    },
    "s067": {
        "cycle_time_ns": 1000000,
        "destinations": [
            "n36"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 24720,
        "redundancy": 1,
        "route": [
            [
                "n40",
                "n15",
                "e67"
            ],
            [
                "n15",
                "n10",
                "e49"
            ],
            [
                "n10",
                "n11",
                "e46"
            ],
            [
                "n11",
                "n36",
                "e50"
            ]
        ],
        "sources": [
            "n40"
        ]
    }
}