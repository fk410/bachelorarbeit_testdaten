{
    "s000": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n91"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n87",
                "n13",
                "e137"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n91",
                "e272"
            ]
        ],
        "sources": [
            "n87"
        ]
    },
    "s001": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n90"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n75",
                "n14",
                "e141"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e212"
            ],
            [
                "n31",
                "n90",
                "e238"
            ]
        ],
        "sources": [
            "n75"
        ]
    },
    "s002": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n87"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e275"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n87",
                "e136"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s003": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e299"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n49",
                "e262"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s004": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n91",
                "n37",
                "e273"
            ],
            [
                "n37",
                "n33",
                "e247"
            ],
            [
                "n33",
                "n38",
                "e248"
            ],
            [
                "n38",
                "n61",
                "e274"
            ]
        ],
        "sources": [
            "n91"
        ]
    },
    "s005": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n90"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e133"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e212"
            ],
            [
                "n31",
                "n90",
                "e238"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s006": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n76"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n88",
                "n19",
                "e171"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n20",
                "e146"
            ],
            [
                "n20",
                "n76",
                "e174"
            ]
        ],
        "sources": [
            "n88"
        ]
    },
    "s007": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e178"
            ],
            [
                "n25",
                "n53",
                "e200"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s008": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n49",
                "e262"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s009": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e269"
            ],
            [
                "n37",
                "n33",
                "e247"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n20",
                "e146"
            ],
            [
                "n20",
                "n58",
                "e172"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s010": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n55",
                "n37",
                "e269"
            ],
            [
                "n37",
                "n33",
                "e247"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n80",
                "e310"
            ]
        ],
        "sources": [
            "n55"
        ]
    },
    "s011": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n79",
                "n38",
                "e277"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e214"
            ],
            [
                "n32",
                "n60",
                "e240"
            ]
        ],
        "sources": [
            "n79"
        ]
    },
    "s012": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n55"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n90",
                "n31",
                "e239"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n55",
                "e268"
            ]
        ],
        "sources": [
            "n90"
        ]
    },
    "s013": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n82"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n63",
                "n12",
                "e129"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n82",
                "e164"
            ]
        ],
        "sources": [
            "n63"
        ]
    },
    "s014": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n72"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n75",
                "n14",
                "e141"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e212"
            ],
            [
                "n31",
                "n72",
                "e236"
            ]
        ],
        "sources": [
            "n75"
        ]
    },
    "s015": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n86",
                "n42",
                "e301"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n65",
                "e196"
            ]
        ],
        "sources": [
            "n86"
        ]
    },
    "s016": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n84"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n78",
                "n32",
                "e243"
            ],
            [
                "n32",
                "n27",
                "e215"
            ],
            [
                "n27",
                "n30",
                "e210"
            ],
            [
                "n30",
                "n84",
                "e232"
            ]
        ],
        "sources": [
            "n78"
        ]
    },
    "s017": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n92"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n85",
                "n36",
                "e267"
            ],
            [
                "n36",
                "n33",
                "e245"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e280"
            ],
            [
                "n43",
                "n92",
                "e306"
            ]
        ],
        "sources": [
            "n85"
        ]
    },
    "s018": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n70",
                "n19",
                "e169"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e138"
            ]
        ],
        "sources": [
            "n70"
        ]
    },
    "s019": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e133"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e212"
            ],
            [
                "n31",
                "n54",
                "e234"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s020": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e235"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n46",
                "e160"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s021": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n91"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n89",
                "n25",
                "e205"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n91",
                "e272"
            ]
        ],
        "sources": [
            "n89"
        ]
    },
    "s022": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n71",
                "n25",
                "e203"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e248"
            ],
            [
                "n38",
                "n61",
                "e274"
            ]
        ],
        "sources": [
            "n71"
        ]
    },
    "s023": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n76",
                "n20",
                "e175"
            ],
            [
                "n20",
                "n15",
                "e147"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n67",
                "e264"
            ]
        ],
        "sources": [
            "n76"
        ]
    },
    "s024": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n83"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n67",
                "n36",
                "e265"
            ],
            [
                "n36",
                "n33",
                "e245"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n83",
                "e198"
            ]
        ],
        "sources": [
            "n67"
        ]
    },
    "s025": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n78"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e133"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e214"
            ],
            [
                "n32",
                "n78",
                "e242"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s026": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n73"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n79",
                "n38",
                "e277"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n73",
                "e270"
            ]
        ],
        "sources": [
            "n79"
        ]
    },
    "s027": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e229"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n52",
                "e166"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s028": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n73"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n70",
                "n19",
                "e169"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n73",
                "e270"
            ]
        ],
        "sources": [
            "n70"
        ]
    },
    "s029": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e173"
            ],
            [
                "n20",
                "n15",
                "e147"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n64",
                "e162"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s030": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n82"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n88",
                "n19",
                "e171"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n82",
                "e164"
            ]
        ],
        "sources": [
            "n88"
        ]
    },
    "s031": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n50"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n67",
                "n36",
                "e265"
            ],
            [
                "n36",
                "n33",
                "e245"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e278"
            ],
            [
                "n42",
                "n50",
                "e296"
            ]
        ],
        "sources": [
            "n67"
        ]
    },
    "s032": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e241"
            ],
            [
                "n32",
                "n27",
                "e215"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n62",
                "e308"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s033": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n71",
                "n25",
                "e203"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n80",
                "e310"
            ]
        ],
        "sources": [
            "n71"
        ]
    },
    "s034": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e163"
            ],
            [
                "n18",
                "n15",
                "e143"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n80",
                "e310"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s035": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n83"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e275"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n83",
                "e198"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s036": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n80",
                "n44",
                "e311"
            ],
            [
                "n44",
                "n39",
                "e283"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e210"
            ],
            [
                "n30",
                "n48",
                "e228"
            ]
        ],
        "sources": [
            "n80"
        ]
    },
    "s037": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n84"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e201"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e210"
            ],
            [
                "n30",
                "n84",
                "e232"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s038": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e210"
            ],
            [
                "n30",
                "n66",
                "e230"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s039": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e235"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n46",
                "e160"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s040": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n85",
                "n36",
                "e267"
            ],
            [
                "n36",
                "n33",
                "e245"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e214"
            ],
            [
                "n32",
                "n60",
                "e240"
            ]
        ],
        "sources": [
            "n85"
        ]
    },
    "s041": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n86",
                "n42",
                "e301"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n86"
        ]
    },
    "s042": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e197"
            ],
            [
                "n24",
                "n21",
                "e177"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n62",
                "e308"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s043": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n92"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n71",
                "n25",
                "e203"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e280"
            ],
            [
                "n43",
                "n92",
                "e306"
            ]
        ],
        "sources": [
            "n71"
        ]
    },
    "s044": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n71"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n80",
                "n44",
                "e311"
            ],
            [
                "n44",
                "n39",
                "e283"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e178"
            ],
            [
                "n25",
                "n71",
                "e202"
            ]
        ],
        "sources": [
            "n80"
        ]
    },
    "s045": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n75"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n46",
                "n18",
                "e161"
            ],
            [
                "n18",
                "n15",
                "e143"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n75",
                "e140"
            ]
        ],
        "sources": [
            "n46"
        ]
    },
    "s046": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n71"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e231"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e178"
            ],
            [
                "n25",
                "n71",
                "e202"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s047": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n67",
                "e264"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s048": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n90"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n89",
                "n25",
                "e205"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e212"
            ],
            [
                "n31",
                "n90",
                "e238"
            ]
        ],
        "sources": [
            "n89"
        ]
    },
    "s049": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e231"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s050": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n79"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n81",
                "n12",
                "e131"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e248"
            ],
            [
                "n38",
                "n79",
                "e276"
            ]
        ],
        "sources": [
            "n81"
        ]
    },
    "s051": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n83"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e139"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n83",
                "e198"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s052": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n90"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e275"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e212"
            ],
            [
                "n31",
                "n90",
                "e238"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s053": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n88"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n58",
                "n20",
                "e173"
            ],
            [
                "n20",
                "n15",
                "e147"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n88",
                "e170"
            ]
        ],
        "sources": [
            "n58"
        ]
    },
    "s054": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n84"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n74",
                "n43",
                "e305"
            ],
            [
                "n43",
                "n39",
                "e281"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e210"
            ],
            [
                "n30",
                "n84",
                "e232"
            ]
        ],
        "sources": [
            "n74"
        ]
    },
    "s055": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s056": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n79",
                "n38",
                "e277"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n20",
                "e146"
            ],
            [
                "n20",
                "n58",
                "e172"
            ]
        ],
        "sources": [
            "n79"
        ]
    },
    "s057": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e275"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e278"
            ],
            [
                "n42",
                "n68",
                "e298"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s058": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n78"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n69",
                "n13",
                "e135"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e214"
            ],
            [
                "n32",
                "n78",
                "e242"
            ]
        ],
        "sources": [
            "n69"
        ]
    },
    "s059": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n68"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n60",
                "n32",
                "e241"
            ],
            [
                "n32",
                "n27",
                "e215"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n42",
                "e278"
            ],
            [
                "n42",
                "n68",
                "e298"
            ]
        ],
        "sources": [
            "n60"
        ]
    },
    "s060": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n65"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n77",
                "n26",
                "e209"
            ],
            [
                "n26",
                "n21",
                "e181"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n65",
                "e196"
            ]
        ],
        "sources": [
            "n77"
        ]
    },
    "s061": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n81"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e235"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n81",
                "e130"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s062": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n90",
                "n31",
                "e239"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n80",
                "e310"
            ]
        ],
        "sources": [
            "n90"
        ]
    },
    "s063": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e195"
            ],
            [
                "n24",
                "n21",
                "e177"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n52",
                "e166"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s064": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n67"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n71",
                "n25",
                "e203"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n67",
                "e264"
            ]
        ],
        "sources": [
            "n71"
        ]
    },
    "s065": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n67",
                "n36",
                "e265"
            ],
            [
                "n36",
                "n33",
                "e245"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e132"
            ]
        ],
        "sources": [
            "n67"
        ]
    },
    "s066": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n79",
                "n38",
                "e277"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n80",
                "e310"
            ]
        ],
        "sources": [
            "n79"
        ]
    },
    "s067": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n88"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n64",
                "n18",
                "e163"
            ],
            [
                "n18",
                "n15",
                "e143"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n88",
                "e170"
            ]
        ],
        "sources": [
            "n64"
        ]
    },
    "s068": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n77"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e180"
            ],
            [
                "n26",
                "n77",
                "e208"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s069": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n88"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n63",
                "n12",
                "e129"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n88",
                "e170"
            ]
        ],
        "sources": [
            "n63"
        ]
    },
    "s070": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n64"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n76",
                "n20",
                "e175"
            ],
            [
                "n20",
                "n15",
                "e147"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n64",
                "e162"
            ]
        ],
        "sources": [
            "n76"
        ]
    },
    "s071": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n70",
                "n19",
                "e169"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n49",
                "e262"
            ]
        ],
        "sources": [
            "n70"
        ]
    },
    "s072": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n45",
                "n12",
                "e127"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e132"
            ]
        ],
        "sources": [
            "n45"
        ]
    },
    "s073": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n91"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n74",
                "n43",
                "e305"
            ],
            [
                "n43",
                "n39",
                "e281"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n91",
                "e272"
            ]
        ],
        "sources": [
            "n74"
        ]
    },
    "s074": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n70",
                "n19",
                "e169"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n49",
                "e262"
            ]
        ],
        "sources": [
            "n70"
        ]
    },
    "s075": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e231"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e248"
            ],
            [
                "n38",
                "n61",
                "e274"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s076": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n60"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n76",
                "n20",
                "e175"
            ],
            [
                "n20",
                "n15",
                "e147"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n32",
                "e214"
            ],
            [
                "n32",
                "n60",
                "e240"
            ]
        ],
        "sources": [
            "n76"
        ]
    },
    "s077": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n87"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e275"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n87",
                "e136"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s078": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n63"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e195"
            ],
            [
                "n24",
                "n21",
                "e177"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n63",
                "e128"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s079": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n91"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e201"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n91",
                "e272"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s080": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n52"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n77",
                "n26",
                "e209"
            ],
            [
                "n26",
                "n21",
                "e181"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n52",
                "e166"
            ]
        ],
        "sources": [
            "n77"
        ]
    },
    "s081": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n51"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n78",
                "n32",
                "e243"
            ],
            [
                "n32",
                "n27",
                "e215"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n51",
                "e132"
            ]
        ],
        "sources": [
            "n78"
        ]
    },
    "s082": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n77"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e231"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n26",
                "e180"
            ],
            [
                "n26",
                "n77",
                "e208"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s083": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e299"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n47",
                "e194"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s084": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n88",
                "n19",
                "e171"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n62",
                "e308"
            ]
        ],
        "sources": [
            "n88"
        ]
    },
    "s085": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n89"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e275"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e178"
            ],
            [
                "n25",
                "n89",
                "e204"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s086": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n58"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e229"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n20",
                "e146"
            ],
            [
                "n20",
                "n58",
                "e172"
            ]
        ],
        "sources": [
            "n48"
        ]
    },
    "s087": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n91"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n67",
                "n36",
                "e265"
            ],
            [
                "n36",
                "n33",
                "e245"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n91",
                "e272"
            ]
        ],
        "sources": [
            "n67"
        ]
    },
    "s088": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n74"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 49440,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n43",
                "e280"
            ],
            [
                "n43",
                "n74",
                "e304"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s089": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n92"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e139"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e280"
            ],
            [
                "n43",
                "n92",
                "e306"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s090": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n66"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n87",
                "n13",
                "e137"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n30",
                "e210"
            ],
            [
                "n30",
                "n66",
                "e230"
            ]
        ],
        "sources": [
            "n87"
        ]
    },
    "s091": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n87"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n73",
                "n37",
                "e271"
            ],
            [
                "n37",
                "n33",
                "e247"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n13",
                "e110"
            ],
            [
                "n13",
                "n87",
                "e136"
            ]
        ],
        "sources": [
            "n73"
        ]
    },
    "s092": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n83"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n92",
                "n43",
                "e307"
            ],
            [
                "n43",
                "n39",
                "e281"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n83",
                "e198"
            ]
        ],
        "sources": [
            "n92"
        ]
    },
    "s093": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n62"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e235"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n62",
                "e308"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s094": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n73"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n54",
                "n31",
                "e235"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n73",
                "e270"
            ]
        ],
        "sources": [
            "n54"
        ]
    },
    "s095": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n82"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n53",
                "n25",
                "e201"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n82",
                "e164"
            ]
        ],
        "sources": [
            "n53"
        ]
    },
    "s096": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n71",
                "n25",
                "e203"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n71"
        ]
    },
    "s097": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n85"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n72",
                "n31",
                "e237"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n85",
                "e266"
            ]
        ],
        "sources": [
            "n72"
        ]
    },
    "s098": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n57"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n66",
                "n30",
                "e231"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n14",
                "e112"
            ],
            [
                "n14",
                "n57",
                "e138"
            ]
        ],
        "sources": [
            "n66"
        ]
    },
    "s099": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n69",
                "n13",
                "e135"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n47",
                "e194"
            ]
        ],
        "sources": [
            "n69"
        ]
    },
    "s100": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n79"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e309"
            ],
            [
                "n44",
                "n39",
                "e283"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e248"
            ],
            [
                "n38",
                "n79",
                "e276"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s101": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n54"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n74",
                "n43",
                "e305"
            ],
            [
                "n43",
                "n39",
                "e281"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n27",
                "e6"
            ],
            [
                "n27",
                "n31",
                "e212"
            ],
            [
                "n31",
                "n54",
                "e234"
            ]
        ],
        "sources": [
            "n74"
        ]
    },
    "s102": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n53"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e299"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n25",
                "e178"
            ],
            [
                "n25",
                "n53",
                "e200"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s103": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n83"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n79",
                "n38",
                "e277"
            ],
            [
                "n38",
                "n33",
                "e249"
            ],
            [
                "n33",
                "n0",
                "e9"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n83",
                "e198"
            ]
        ],
        "sources": [
            "n79"
        ]
    },
    "s104": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n48"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n84",
                "n30",
                "e233"
            ],
            [
                "n30",
                "n48",
                "e228"
            ]
        ],
        "sources": [
            "n84"
        ]
    },
    "s105": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n70"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n87",
                "n13",
                "e137"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n70",
                "e168"
            ]
        ],
        "sources": [
            "n87"
        ]
    },
    "s106": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n74"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n52",
                "n19",
                "e167"
            ],
            [
                "n19",
                "n15",
                "e145"
            ],
            [
                "n15",
                "n0",
                "e3"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e280"
            ],
            [
                "n43",
                "n74",
                "e304"
            ]
        ],
        "sources": [
            "n52"
        ]
    },
    "s107": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n79"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e195"
            ],
            [
                "n24",
                "n21",
                "e177"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e248"
            ],
            [
                "n38",
                "n79",
                "e276"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s108": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e303"
            ],
            [
                "n43",
                "n39",
                "e281"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n49",
                "e262"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s109": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n79"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 20736,
        "redundancy": 1,
        "route": [
            [
                "n61",
                "n38",
                "e275"
            ],
            [
                "n38",
                "n79",
                "e276"
            ]
        ],
        "sources": [
            "n61"
        ]
    },
    "s110": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n47"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n68",
                "n42",
                "e299"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n47",
                "e194"
            ]
        ],
        "sources": [
            "n68"
        ]
    },
    "s111": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n45"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n56",
                "n43",
                "e303"
            ],
            [
                "n43",
                "n39",
                "e281"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n45",
                "e126"
            ]
        ],
        "sources": [
            "n56"
        ]
    },
    "s112": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n46"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n83",
                "n24",
                "e199"
            ],
            [
                "n24",
                "n21",
                "e177"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n18",
                "e142"
            ],
            [
                "n18",
                "n46",
                "e160"
            ]
        ],
        "sources": [
            "n83"
        ]
    },
    "s113": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n65",
                "n24",
                "e197"
            ],
            [
                "n24",
                "n21",
                "e177"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n49",
                "e262"
            ]
        ],
        "sources": [
            "n65"
        ]
    },
    "s114": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n83"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n50",
                "n42",
                "e297"
            ],
            [
                "n42",
                "n39",
                "e279"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n21",
                "e4"
            ],
            [
                "n21",
                "n24",
                "e176"
            ],
            [
                "n24",
                "n83",
                "e198"
            ]
        ],
        "sources": [
            "n50"
        ]
    },
    "s115": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n49"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n78",
                "n32",
                "e243"
            ],
            [
                "n32",
                "n27",
                "e215"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n49",
                "e262"
            ]
        ],
        "sources": [
            "n78"
        ]
    },
    "s116": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n73"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n63",
                "n12",
                "e129"
            ],
            [
                "n12",
                "n9",
                "e109"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n73",
                "e270"
            ]
        ],
        "sources": [
            "n63"
        ]
    },
    "s117": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n74"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n57",
                "n14",
                "e139"
            ],
            [
                "n14",
                "n9",
                "e113"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n43",
                "e280"
            ],
            [
                "n43",
                "n74",
                "e304"
            ]
        ],
        "sources": [
            "n57"
        ]
    },
    "s118": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n81"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n47",
                "n24",
                "e195"
            ],
            [
                "n24",
                "n21",
                "e177"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n9",
                "e0"
            ],
            [
                "n9",
                "n12",
                "e108"
            ],
            [
                "n12",
                "n81",
                "e130"
            ]
        ],
        "sources": [
            "n47"
        ]
    },
    "s119": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n61"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n62",
                "n44",
                "e309"
            ],
            [
                "n44",
                "n39",
                "e283"
            ],
            [
                "n39",
                "n0",
                "e11"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n38",
                "e248"
            ],
            [
                "n38",
                "n61",
                "e274"
            ]
        ],
        "sources": [
            "n62"
        ]
    },
    "s120": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n88"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n51",
                "n13",
                "e133"
            ],
            [
                "n13",
                "n9",
                "e111"
            ],
            [
                "n9",
                "n0",
                "e1"
            ],
            [
                "n0",
                "n15",
                "e2"
            ],
            [
                "n15",
                "n19",
                "e144"
            ],
            [
                "n19",
                "n88",
                "e170"
            ]
        ],
        "sources": [
            "n51"
        ]
    },
    "s121": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n80"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n89",
                "n25",
                "e205"
            ],
            [
                "n25",
                "n21",
                "e179"
            ],
            [
                "n21",
                "n0",
                "e5"
            ],
            [
                "n0",
                "n39",
                "e10"
            ],
            [
                "n39",
                "n44",
                "e282"
            ],
            [
                "n44",
                "n80",
                "e310"
            ]
        ],
        "sources": [
            "n89"
        ]
    },
    "s122": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n73"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n90",
                "n31",
                "e239"
            ],
            [
                "n31",
                "n27",
                "e213"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n37",
                "e246"
            ],
            [
                "n37",
                "n73",
                "e270"
            ]
        ],
        "sources": [
            "n90"
        ]
    },
    "s123": {
        "cycle_time_ns": 500000,
        "destinations": [
            "n85"
        ],
        "frame_size_b": 100,
        "max_latency_ns": 78144,
        "redundancy": 1,
        "route": [
            [
                "n48",
                "n30",
                "e229"
            ],
            [
                "n30",
                "n27",
                "e211"
            ],
            [
                "n27",
                "n0",
                "e7"
            ],
            [
                "n0",
                "n33",
                "e8"
            ],
            [
                "n33",
                "n36",
                "e244"
            ],
            [
                "n36",
                "n85",
                "e266"
            ]
        ],
        "sources": [
            "n48"
        ]
    }
}