#!/usr/bin/env python3
import json 
import sys

# file with hand picked random bytes
rndfile = "rnd"
# parameter runs in json
scenariofile = "scenarios.json"
# measurement repetitons
repeat = 5

# template for starting a scheduling run
template = '''#!/bin/bash
${{COMMAND}} --topology=${BENCHMARK_PATH}/topos/{top} \\
           --streamset=${BENCHMARK_PATH}/streams/{ss} \\
           --schedule=${{scd}} \\
           --check=${{chk}} \\
           --seed={seed} \\
           --timeout=${{TIMEOUT}}
'''

# create 8 byte ulong random numbers from random file
def rnd(filename):
    with open(filename,'rb') as f:
        while True:
            b = f.read(8)
            r = int.from_bytes(b, 'big', signed=False)
            yield r
rnd = rnd(rndfile)

# create exp file
def exp(top,n,sss,ct,fs,a,ss,i):
    seed = next(rnd)
    name = f'{top}_{n:02d}_sss{sss:03d}_ct{ct:04d}_fs{fs:04d}_a{a}_ss{ss:02d}_{i:02d}.exp'
    vals = {
        'top': f'{top}_{n:02d}.json',
        'ss': f'{top}_{n:02d}_sss{sss:03d}_ct{ct:04d}_fs{fs:04d}_a{a}_ss{ss:02d}.pat',
        'seed' : f'{seed}',
    }
    text = template.format(template, **vals)
    with open(name,"w") as f:
        f.write(text)

# iterate over all scenarios
def iter(scenarios, i):
    for s in scenarios:
        for args in scenarios[s]:
            for top in args[0]:
                for n in args[1]:
                    for sss in args[2]:
                        for ct in args[3]:
                            for fs in args[4]:
                                for a in args[5]:
                                    for ss in range(4):
                                        exp(top,n,sss,ct,fs,a,ss,i) 

# load scenario file
# create scheduling run for parameter combination
# repeat for multiple measurements
def main():    
    with open(scenariofile, 'r') as f:
        j = json.load(f)       
        for i in range(repeat):
            iter(j,i)
               
if __name__ == '__main__':
    sys.exit(main())
        