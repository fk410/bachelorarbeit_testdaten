#!/usr/bin/env python3

# dependencies

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json
import math

####################################################################################
# benchmark functions

def benchmark_function(data):

    ###################################################################################################################
    # set variables for the calculation

    # configure how how exactly the json is broken down for the test files
    JSON_config_file_score      =   True    # if True create a nested JSON for each testfile , If False only save benchmark score
    JSON_config_partial_score   =   False   # if True break down the score of each file each testfile , If False only save file total score

    # variables for the time score calculation
    calculate_time_score        =   True    # if True then calculate time score, If False skip calculation
    score_time_interval_min     =   900     # Score if calculation is completed at maximum time (without timeout) (lower limit of the interval)
    score_time_interval_max     =   1800    # score at 0 seconds calculation time (upper limit of the interval)
    score_penalty               =   300     # penalty for each not calculated stream per schedule
    timeout_time                =   900     # max timeout time, assignment of the score linearly between 0 and max_timeout
    timeout_time_toleranz       =   15      # Tolerance time that goes beyond the timeout and is still scored as having a minimum score


    # variables for the memory score calculation
    calculate_memory_score      =   False   # if True then calculate memory score, If False skip calculation
    score_memory_max            =   500     # default score by Guide value at max memory allocation
    score_memory_avg            =   500     # default score by Guide value at avg memory allocation

    ###################################################################################################################
    # start calculation with iteration over the all schedules and checks

    # init json
    Json                                    =   {}
    Json["benchmark_score"]                 =   0
    benchmark_score_total                   =   0

    for i in range(len(data)):

        # check time of the scheduler
        # read dataframe
        filename            = data.index.values[i]
        result_check        = data["result_check"].iloc[i]
        coverage            = data["result_coverage"].iloc[i]
        streams_scheduled   = data["streams_scheduled"].astype(float).iloc[i]
        streams_provided    = data["streams_provided"].astype(float).iloc[i]
        scheduler_time      = data["SCHEDULER_RUN_TIME"].astype(float).iloc[i]
        memory_max          = data["SCHEDULER_RAM_MAX"].astype(int).iloc[i]
        memory_avg          = data["SCHEDULER_RAM_AVG"].astype(int).iloc[i]

        # calculate score only if scheduler calculation is success
        # if calculation of the scheduler is not successful set score to 0 and save error
        if result_check == "PASSED":
            # reset and init score
            partial_score_time = 0
            partial_score_memory = 0

            ###########################################################################################################
            # calculate time score
            if calculate_time_score:

                # calculate time score
                if (coverage=="FULL") or (coverage=="PARTIAL"):
                    # calculate time multiplier
                    # check if the time is less than the timeout with tolerance
                    if scheduler_time < (timeout_time + timeout_time_toleranz):
                        # check if the time is less than the timeout
                        if scheduler_time < timeout_time:
                            # calculate multiplier with the schedule time and the timeout
                            time_multiplicator = 1 - (scheduler_time / timeout_time)
                        else:
                            # if measured time is above Timeout time, but within tolerance, set multiplicator to 0
                            time_multiplicator=0
                        score_time_interval_range = score_time_interval_max - score_time_interval_min
                        # calculate score for each using the benchmark and differentiator
                        achieved_time_score = score_time_interval_range * time_multiplicator + score_time_interval_min
                        partial_score_time += int(achieved_time_score)
                    # If timeout is above tolerance set Score to 0
                    else:
                        achieved_time_score = 0
                        partial_score_time += int(achieved_time_score)

                # calculate score penalty
                if (coverage=="NONE") or (coverage=="PARTIAL"):
                    # calculate penalty for every uncalculated stream
                    score_penalty_total= (streams_provided - streams_scheduled) * score_penalty
                    partial_score_time-= score_penalty_total

                # if partial_score_time is negative set to 0
                if partial_score_time < 0:
                    partial_score_time = 0

            ###########################################################################################################
            # calculate score for memory usage
            if  calculate_memory_score:
                if memory_max <= 0:
                    log_memory_max = 1
                else:
                    log_memory_max = math.log10(memory_max) + 1

                if memory_avg <= 0:
                    log_memory_avg = 1
                else:
                    log_memory_avg  = math.log10(memory_avg) + 1

                memory_max_score = score_memory_max / log_memory_max
                memory_avg_score = score_memory_avg / log_memory_avg
                partial_score_memory = memory_max_score + memory_avg_score

            ###########################################################################################################
            # add all partial scores to file score
            partial_score = partial_score_time + partial_score_memory
            # round score and add too benchmark total score
            benchmark_score_total += int(partial_score)

        ###########################################################################################################
        # if calculation of the scheduler is not successful set score to 0 and save error
        else:
            # set score and partial score to 0
            partial_score=0
            partial_score_time=0
            partial_score_memory=0

        ###########################################################################################################
        # initialize Json entry for file and and fill it in according to the config with the partial scores
        if JSON_config_file_score :
            if JSON_config_partial_score :
                Json[f"{filename}_score"] = {}
                Json[f"{filename}_score"]["partial_score"] = int(partial_score)
                Json[f"{filename}_score"]["itemized"] = {}
                if calculate_time_score:
                    Json[f"{filename}_score"]["itemized"]["time_score"] = int(partial_score_time)
                if  calculate_memory_score:
                    Json[f"{filename}_score"]["itemized"]["memory_score"] = int(partial_score_memory)
            else:
                Json[f"{filename}_score"] = int(partial_score)

    ###########################################################################################################
    # calculate benchmark score

    ##### average benchmark, uncomment if to be used and comment other alternatives
    #benchmark_score_avg = benchmark_score_total / len(data.index)
    #benchmark_score_avg = int(benchmark_score_avg)
    #Json["benchmark_score"] = benchmark_score_avg

    ##### percentage benchmark, uncomment if to be used and comment other alternatives
    #benchmark_score_per = benchmark_score_total / (len(data.index)*score_time_interval_max)
    #benchmark_score_per = 100*round(benchmark_score_per, 4)
    #Json["benchmark_score"] = benchmark_score_per

    ##### set sum off all benchmarks as score, uncomment if to be used and comment other alternatives
    Json["benchmark_score"] = benchmark_score_total

    ###########################################################################################################
    # return JSON
    return Json
