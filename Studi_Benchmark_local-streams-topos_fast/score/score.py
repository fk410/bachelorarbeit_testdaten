#!/usr/bin/env python3

# dependencies

from pickle import FALSE, TRUE
import pandas as pd
import numpy as np
import os
import os.path
import sys
import json
import datetime
import score_func as sf

####################################################################################
# initialize

# Delimiter for reading files
DELIMITER="="

# read input from environment variable ( not in Studi_Benchmark )
JOBS_DIRECTORY=os.environ["JOBS_DIRECTORY"]
SCHEDULER_NAME=os.environ["SCHEDULER_NAME"]
SCHEDULER_VERSION=os.environ["SCHEDULER_VERSION"]
SCHEDULER_IMAGE=os.environ["SCHEDULER_IMAGE"]
CHECKER_IMAGE=os.environ["CHECKER_IMAGE"]
TESTDATA=os.environ["TESTDATA"]
SCHEDULER_CONTAINER=os.environ["SCHEDULER_CONTAINER"]
CHECKER_CONTAINER=os.environ["CHECKER_CONTAINER"]
BSYS_DIRECTORY=os.environ["BSYS_DIRECTORY"]
BSYS_DIRECTORY=f'{BSYS_DIRECTORY}/jobs/benchmark/'
HARDWARE=os.environ["HARDWARE"]

####################################################################################
# changeable variables and examples for filter function

# set pattern for chk, if .chk is not readable (0 Points) 
CHK_FAIL={
  "result": {
    "check": "FAILED",
    "coverage": "NONE"
  },
  "streams": {
    "scheduled": 0,
    "provided": 1
  },
  "errors": ["chk failure"],
  "warnings": []
}
# set pattern for mst, if .mst file is not readable (0 Points and set CHK_FAIL)
MST_FAIL={
  "SCHEDULER_RUN_TIME": "0.00",
  "SCHEDULER_CPU_USAGE": "0%",
  "SCHEDULER_RAM_AVG": "0",
  "SCHEDULER_RAM_MAX": "0",
  "SCHEDULER_RAM_UNIT": "Kbytes",
  "SCHEDULER_EXIT_CODE": "0",
  "SCHEDULER_TIMEOUT": "FALSE"
}
# init error list
JSON_errors=[]

####################################################################################
# filter functions

# User defined filter function, filters input from every mst file
# filter before connecting, filter every mst file separate
# every variable saved as column
def prefilter(data):
    return data

# User defined filter function
# filter on connected data frame
# every variable saved as column
def linkedfilter(data):
    data = data.sort_index()
    return data

def flatten_json(nested_json):
    """
        Flatten json object with nested keys into a single level.
        Args:
            nested_json: A nested json object.
        Returns:
            The flattened json object if successful, None otherwise.
    """
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        else:
            out[name[:-1]] = x

    flatten(nested_json)
    return out


####################################################################################
# read in and filter

# initialize
SAVE=[]


# for every file in DIRECTORY with end .mst read out file and save them in array SAVE
for filename in os.listdir(JOBS_DIRECTORY):
    if filename.endswith(".exp"):
        tmp_df_Save=[]
        # save chk filename
        filename_split=os.path.splitext(filename)[0]
        filename_mst=filename_split+".mst"
        filename_chk=filename_split+".chk"
        # reset mast file 
        mst_valid=True

        if os.path.isfile(f'{JOBS_DIRECTORY}/{filename_mst}'):
            # read out .mst files, if failure set pattern for mst and chk for 0 points
            with open(f'{JOBS_DIRECTORY}/{filename_mst}') as f:
                try: 
                    tmp_mst = json.load(f)
                except:
                    error_message=f"{filename_mst} not in json format"
                    JSON_errors.append(f'{error_message}')
                    mst_valid=False
                    tmp_mst=MST_FAIL
                    tmp_chk=CHK_FAIL
        else:
            error_message=f"{filename_mst} does not exist"
            JSON_errors.append(error_message)
            mst_valid=False
            tmp_mst=MST_FAIL
            tmp_chk=CHK_FAIL

        # if mst is valid read out .chk file
        if mst_valid:
            if os.path.isfile(f'{JOBS_DIRECTORY}/{filename_chk}'):
            # read out .chk files, if failure set pattern for chk for 0 points
                with open(f'{JOBS_DIRECTORY}/{filename_chk}') as f:
                    try:
                        tmp_chk = json.load(f)
                    except:
                        error_message=f"{filename_chk} not in json format"
                        JSON_errors.append(error_message)
                        tmp_chk=CHK_FAIL
            else:
                error_message=f"{filename_chk} does not exist"
                JSON_errors.append(error_message)
                tmp_chk=CHK_FAIL

        # convert mst file to dataframe
        tmp_df_mst = pd.Series(tmp_mst).to_frame()
        # flatten tmp_chk and convert to dataframe
        tmp_df_chk = pd.Series(flatten_json(tmp_chk)).to_frame()

        # combine mst and chk dataframe
        tmp_df_Save+=[tmp_df_chk]
        tmp_df_Save+=[tmp_df_mst]
        tmp_df = pd.concat(tmp_df_Save)

        # transpose matrix
        tmp_df = tmp_df.transpose()

        # run defined filter function (see: def prefilter(data):)
        tmp_df = prefilter(tmp_df)

        # set the filename as index
        tmp_df = tmp_df.rename(index={0 : f"{filename_split}"})

        # save data frame in array for connecting
        SAVE+=[tmp_df]


# connect data frames saved in array SAVE
DATA = pd.concat(SAVE)

# user defined filter
DATA = linkedfilter(DATA)


####################################################################################
# create json
#BENCHMARK = benchmark_function(data)

# call the score calculation function
benchmark_json = sf.benchmark_function(DATA)

# get current time
date=datetime.datetime.now()
date=date.isoformat()

jsondata = {}
jsondata["scheduler_name"] = SCHEDULER_NAME
jsondata["scheduler_version"] = SCHEDULER_VERSION
jsondata["scheduler_image"] = SCHEDULER_IMAGE
jsondata["checker_image"] = CHECKER_IMAGE
jsondata["testdata_version"] = TESTDATA
jsondata["scheduler_container"] = SCHEDULER_CONTAINER
jsondata["checker_container"] = CHECKER_CONTAINER
jsondata["date"] = date
jsondata["hardware"] = HARDWARE
jsondata["benchmark"] = benchmark_json
if len(JSON_errors) != 0:
    jsondata["errors"] = JSON_errors

with open(f'{BSYS_DIRECTORY}/{SCHEDULER_NAME}_{SCHEDULER_VERSION}.json', 'w', encoding='utf-8') as f:
    json.dump(jsondata, f, ensure_ascii=False, indent=4)
